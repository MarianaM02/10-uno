package ejercicio1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class CuentaPalabra {

	/**
	 * post: Dado un archivo de entrada, un archivo de salida, y un arreglo de palabras,
	 * lea las palabras desde el archivo de entrada, y en el archivo de salida
	 * escriba la cantidad de veces que aparece en el archivo de entrada cada
	 * palabra presente en el arreglo. Se considera la comparacion literal de los string
	 * 
	 * @param archivoEntrada
	 * @param archivoSalida
	 * @param palabras       Array de palabras a contar
	 * @throws IOException
	 */
	public void cuentaPalabras(String archivoEntrada, String archivoSalida, String[] palabras) throws IOException {
		File archivo = new File(archivoEntrada);
		Scanner lector = new Scanner(archivo);
		int[] arrContador = new int[palabras.length];
		String cadenaCompleta = "";
		String cadenaCompletaSinPuntuacion;
		String[] arrPalabrasArchivo;
		ArrayList<String> listaPalabras;

		while (lector.hasNext()) {
			cadenaCompleta += lector.nextLine();
		}
		lector.close();

		cadenaCompletaSinPuntuacion = limpiarCadena(cadenaCompleta);
		arrPalabrasArchivo = cadenaCompletaSinPuntuacion.split(" ");
		listaPalabras = new ArrayList<String>(Arrays.asList(arrPalabrasArchivo));

		FileWriter archivoOut = new FileWriter(archivoSalida);
		PrintWriter salida = new PrintWriter(archivoOut);
		for (int i = 0; i < palabras.length; i++) {
			arrContador[i] = Collections.frequency(listaPalabras, palabras[i]);
			salida.println(palabras[i] + " " + arrContador[i]);
		}
		salida.close();

	}

	/**
	 * post: Se string en minuscula y libre de puntuaciones . , ( )
	 * 
	 * @param cadena String a limpiar
	 * @return
	 */
	private String limpiarCadena(String cadena) {
		String cadenaCompletaSinPuntuacion;
		cadenaCompletaSinPuntuacion = cadena.replace(",", "");
		cadenaCompletaSinPuntuacion = cadenaCompletaSinPuntuacion.replace(".", " ");
		cadenaCompletaSinPuntuacion = cadenaCompletaSinPuntuacion.replace("(", "").replace(")", "");
		cadenaCompletaSinPuntuacion = cadenaCompletaSinPuntuacion.toLowerCase();
		return cadenaCompletaSinPuntuacion;
	}

	public String[] obtenerArregloDePalabras() {
		// modificarlo si fuera necesario para hacer m�s y mejores pruebas
		String[] s = { "vim", "unix", "editor", "modo", "�rdenes" };
		return s;
	}

	public static void main(String[] args) throws IOException {
		CuentaPalabra contador = new CuentaPalabra();
		contador.cuentaPalabras("src/ejercicio1/archivoDeEntrada.in", "src/ejercicio1/archivoDeSalida.out",
				contador.obtenerArregloDePalabras());
	}

}
