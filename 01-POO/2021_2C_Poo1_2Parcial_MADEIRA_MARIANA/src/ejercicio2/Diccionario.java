package ejercicio2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Diccionario {

	/**
	 * post: Toma un texto en castellano y un diccionario, y devuelve la oraci�n
	 * traducida palabra por palabra seg�n el diccionario. Si una palabra no se
	 * encuentra en el diccionario, deber� sustituirla por <error> en la cadena
	 * resultante.
	 * 
	 * @param texto       String a Traducir
	 * @param diccionario Mapa con palabras en espa�ol traducidas al ingl�s
	 * @return
	 */
	public String traducir(String texto, Map<String, String> diccionario) {
		String textoTraducido = "";
		List<String> listaStr = Arrays.asList(texto.toLowerCase().split(" "));

		for (String string : listaStr) {
			textoTraducido += diccionario.getOrDefault(string, "<error>") + " ";
		}

		return textoTraducido;
	}

	public Map<String, String> obtenerDiccionario() {
		// modificarlo si fuera necesario para hacer m�s y mejores pruebas
		Map<String, String> diccionario = new HashMap<String, String>();
		diccionario.put("amistad", "friendship");
		diccionario.put("la", "the");
		diccionario.put("el", "the");
		diccionario.put("one", "uno");
		diccionario.put("vida", "life");
		diccionario.put("buenos", "good");
		diccionario.put("importantes", "important");
		diccionario.put("m�s", "most");
		diccionario.put("tener", "to have");
		diccionario.put("pueden", "can");
		diccionario.put("dif�ciles", "difficult");

		return diccionario;
	}

	public static void main(String[] args) {
		Diccionario dicc = new Diccionario();
		// modificarlo si fuera necesario para hacer m�s y mejores pruebas
		String texto = "La amistad es uno de los valores m�s importantes " + "que se pueden tener en la vida";

		String textoTraducido = dicc.traducir(texto, dicc.obtenerDiccionario());

		System.out.println(textoTraducido);
	}

}
