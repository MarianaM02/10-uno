package ejercicio3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CuentaNumero {

	/**
	 * post: Dado un archivo, que puede contener n�meros y palabras separados por
	 * espacios, calcule la cantidad de n�meros encontrados en el archivo. El m�todo
	 * debe tratar las excepciones que puedan producirse, mostrando mensajes de
	 * error apropiados en cada caso.
	 * 
	 * @param texto String
	 * @return
	 */
	public Integer contarNumeros(String texto) {
		Integer cantidadNumeros = 0;
		String[] arrPalabrasArchivo;

		arrPalabrasArchivo = texto.split(" ");
		for (String string : arrPalabrasArchivo) {
			try {
				Integer.parseInt(string);
				cantidadNumeros++;
			} catch (NumberFormatException e) {
				System.out.println("NumberFormatException " + e.getMessage());
				e.getStackTrace();
			}
		}
		return cantidadNumeros;
	}

	public String obtenerTextoDeArchivo(String archivoEntrada) throws FileNotFoundException {
		String texto = "";
		File archivo = new File(archivoEntrada);
		Scanner lector = new Scanner(archivo);

		while (lector.hasNext()) {
			texto += lector.nextLine() + " ";
		}
		lector.close();

		return texto;
	}

	public static void main(String[] args) throws FileNotFoundException {
		CuentaNumero c = new CuentaNumero();

		System.out.println(c.contarNumeros(c.obtenerTextoDeArchivo("src/ejercicio3/texto.in")));
	}
}
