package ejercicio4;

import java.util.Objects;

public class Desarrollador {
	private int nroLegajo;
	private String nombre;
	private double sueldo;
	private Seniority seniority;
	
	public Desarrollador(int nroLegajo, String nombre, double sueldo, Seniority seniority) {
		super();
		this.nroLegajo = nroLegajo;
		this.nombre = nombre;
		this.sueldo = sueldo;
		this.seniority = seniority;
	}

	public int getNroLegajo() {
		return nroLegajo;
	}

	public String getNombre() {
		return nombre;
	}

	public double getSueldo() {
		return sueldo;
	}

	public Seniority getSeniority() {
		return seniority;
	}

	@Override
	public int hashCode() {
		return Objects.hash(nombre, nroLegajo, seniority, sueldo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Desarrollador other = (Desarrollador) obj;
		return Objects.equals(nombre, other.nombre) && nroLegajo == other.nroLegajo && seniority == other.seniority
				&& Double.doubleToLongBits(sueldo) == Double.doubleToLongBits(other.sueldo);
	}

	@Override
	public String toString() {
		return "[" + nroLegajo + "	" + nombre + "	$" + sueldo + "	"
				+ seniority + "]";
	}

}
