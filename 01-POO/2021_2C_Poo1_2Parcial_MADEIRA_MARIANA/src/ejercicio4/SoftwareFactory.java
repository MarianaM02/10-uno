package ejercicio4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 * Implementar la clase {@link Desarrollador}, teniendo en cuenta los atributos
 * n�mero de legajo, nombre, sueldo y seniority. El {@link Seniority} puede ser
 * Junior, SemiSenior o Senior, cualquier otra calificaci�n no es v�lida.
 * Implementar la clase SoftwareFactory, la cual contendr� un conjunto de
 * desarrolladores; l�gicamente no puede contener dos veces al mismo
 * desarrollador.
 * 
 */
public class SoftwareFactory {

	private HashSet<Desarrollador> desarrolladores;

	public SoftwareFactory() {
		super();
		this.desarrolladores = new HashSet<Desarrollador>();
	}

	/**
	 * post: Retorna una lista de los desarolladores, ordenados seg�n el sueldo que
	 * cobran de forma descendente utilizando {@link SueldoComparator}
	 *
	 */
	public List<Desarrollador> listarDesarrolladoresSegunSueldo() {
		List<Desarrollador> lista = new ArrayList<Desarrollador>();
		desarrolladores.forEach(d -> lista.add(d));
		Collections.sort(lista, new SueldoComparator());
		return lista;
	}

	/**
	 * post: Modifica la lista de desarrolladores, tal que s�lo queden los
	 * desarrolladores que sean SemiSenior. Se considera que este metodo debe
	 * alterar el estado de {@link SoftwareFactory}
	 */
	public void seleccionarSemiSeniors() {
		Iterator<Desarrollador> itr = desarrolladores.iterator();
		while (itr.hasNext()) {
			Desarrollador des = itr.next();
			if (!des.getSeniority().equals(Seniority.SEMI_SENIOR)) {
				itr.remove();
			}
		}
	}

	public boolean agregarDesarrollador(Desarrollador des) {
		return this.desarrolladores.add(des);
	}

	public HashSet<Desarrollador> getDesarrolladores() {
		return desarrolladores;
	}

	public static void main(String[] args) {
		SoftwareFactory sf = new SoftwareFactory();

		sf.agregarDesarrollador(new Desarrollador(1234, "Laura", 100000, Seniority.JUNIOR));
		sf.agregarDesarrollador(new Desarrollador(1234, "Mauro", 120000, Seniority.SEMI_SENIOR));
		sf.agregarDesarrollador(new Desarrollador(1234, "Agus", 200000, Seniority.SENIOR));
		sf.agregarDesarrollador(new Desarrollador(1234, "Eli", 130000, Seniority.JUNIOR));
		sf.agregarDesarrollador(new Desarrollador(1234, "Laura", 100000, Seniority.JUNIOR));
		sf.agregarDesarrollador(new Desarrollador(1234, "Agu", 110000, Seniority.SEMI_SENIOR));

		System.out.println("******************* Desarrolladores *********************");
		sf.getDesarrolladores().forEach(d -> System.out.println(d));
		System.out.println("********************* Por Sueldo ************************");
		sf.listarDesarrolladoresSegunSueldo().forEach(d -> System.out.println(d));
		System.out.println("******************** Semi-Seniors ***********************");
		sf.seleccionarSemiSeniors();
		sf.getDesarrolladores().forEach(d -> System.out.println(d));

	}
}
