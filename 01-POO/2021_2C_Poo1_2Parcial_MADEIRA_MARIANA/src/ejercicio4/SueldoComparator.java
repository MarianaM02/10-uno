package ejercicio4;

import java.util.Comparator;

/**
 * Compara sueldos de mayor a menor
 * 
 */
public class SueldoComparator implements Comparator<Desarrollador> {

	@Override
	public int compare(Desarrollador o1, Desarrollador o2) {
		return Double.compare(o2.getSueldo(), o1.getSueldo());
	}

}
