package camping;

import java.util.ArrayList;

public class Camping {
	private ArrayList<Parcela> parcelas;

	public Camping(int cantTotalParcelas) {
		this.parcelas = new ArrayList<Parcela>(cantTotalParcelas);
		for (int i=0; i<cantTotalParcelas; i++) {
			parcelas.add(i, new Parcela());
		}
	}

	public ArrayList<Parcela> getParcelas() {
		return parcelas;
	}

	/**
	 * pre: Parcelas vacantes
	 * 
	 * post: Ocupar una parcela disponible indicando la cantidad de personas
	 * mayores y menores (m�ximo 12 en total) que la ocupan.
	 * 
	 * @param mayores: Cant de personas adultas
	 * @param menores: Cant de personas menores de edad
	 */
	public void ocuparParcelaCon(int mayores, int menores) {
		if (!hayVacantes()) {
			throw new Error("No hay m�s vacantes!");
		}
		for (Parcela p : parcelas) {
			if (p.ocupar(mayores, menores)) {
				return;
			}
		}
	}

	/**
	 * post: Devuelve la cantidad total de personas que ocupan todas las
	 * parcelas del camping.
	 */
	public int contarTotalAcampantes() {
		int totalAcampantes = 0;
		for (Parcela p : parcelas) {
			totalAcampantes += p.calcularTotal();
		}
		return totalAcampantes;
	}

	/**
	 * post: Devuelve la cantidad de parcelas que est�n ocupadas por tantos
	 * mayores como los indicados por par�metro.
	 * 
	 * @param mayores: Valor a comparar (cantidad de personas mayores)
	 */
	public int contarParcelasCon(int mayores) {
		int cantParcelas = 0;
		for (Parcela p : parcelas) {
			int n = p.getCantMayores();
			if (n == mayores) {
				cantParcelas++;
			}
		}
		return cantParcelas;
	}

	/**
	 * post: Devuelve un arreglo de enteros de longitud 9, tal que en la posici�n i
	 * del arreglo se guarde la cantidad de parcelas con i personas.
	 */
	public int[] contarParcelasPorCantAcampantes() {
		int[] arr = new int[9];
		for (Parcela p : parcelas) {
			int i = p.calcularTotal();
			arr[i]++;
		}
		return arr;
	}

	public boolean hayVacantes() {
		int ultimaParcela = parcelas.size()-1 ;
		return parcelas.get(ultimaParcela).estaVacia();
	}

	@Override
	public String toString() {
		return "Camping [parcelas=" + parcelas + "]";
	}

}
