package camping;

public class Parcela {

	public static final int MAX_ACAMPANTES = 12;
	private int cantMayores;
	private int cantMenores;

	public int getCantMayores() {
		return cantMayores;
	}

	public int getCantMenores() {
		return cantMenores;
	}

	public boolean ocupar(int mayores, int menores) {
		if (mayores < 1 || menores < 0) {
			throw new Error("Valores inv�lidos: Deben ser valores " + "positivos y debe haber al menos un Mayor");
		}
		if ((mayores + menores) > MAX_ACAMPANTES) {
			throw new Error("Valores inv�lidos: El m�ximo de " + "Acampantes por Parcela es " + MAX_ACAMPANTES);
		}

		if (estaVacia()) {
			this.cantMayores = mayores;
			this.cantMenores = menores;
			return true;
		}
		return false;

	}

	public int calcularTotal() {
		return cantMayores + cantMenores;
	}

	public boolean estaVacia() {
		return calcularTotal() == 0;
	}

	@Override
	public String toString() {
		return "\nParcela [cantMayores=" + cantMayores + ", cantMenores=" + cantMenores + "]";
	}

}
