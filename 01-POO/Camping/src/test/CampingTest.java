package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import camping.Camping;
import camping.Parcela;

public class CampingTest {

	Camping camping;

	@Before
	public void setUp() throws Exception {
		camping = new Camping(6);
	}

	@Test
	public void crearCampingTest() {
		assertNotNull(camping);
		for (Parcela p : camping.getParcelas()) {
			assertEquals(p.getCantMayores(), 0);
			assertEquals(p.getCantMenores(), 0);
		}
	}

	@Test
	public void ocuparParcelaTest() {
		camping.ocuparParcelaCon(2, 3);
		camping.ocuparParcelaCon(6, 2);
		ArrayList<Parcela> lista = camping.getParcelas();
		assertEquals(lista.get(0).getCantMayores(), 2);
		assertEquals(lista.get(0).getCantMenores(), 3);
		assertEquals(lista.get(1).getCantMayores(), 6);
		assertEquals(lista.get(1).getCantMenores(), 2);
	}

	@Test
	public void contarTotalAcampantes() {
		int obtenido = camping.contarTotalAcampantes();
		int esperado = 0;
		assertEquals(obtenido, esperado);
		camping.ocuparParcelaCon(2, 3);
		camping.ocuparParcelaCon(6, 2);
		obtenido = camping.contarTotalAcampantes();
		esperado = 13;
		assertEquals(obtenido, esperado);
	}

	@Test
	public void contarParcelasConTest() {
		camping.ocuparParcelaCon(2, 3);
		camping.ocuparParcelaCon(6, 2);
		
		int obtenido = camping.contarParcelasCon(2);
		int esperado = 1;
		assertEquals(obtenido, esperado);

		obtenido = camping.contarParcelasCon(0);
		esperado = 4;
		assertEquals(obtenido, esperado);

		obtenido = camping.contarParcelasCon(5);
		esperado = 0;
		assertEquals(obtenido, esperado);
	}

	@Test
	public void contarParPorCantAcampantes() {
		int[] arrObtenido = camping.contarParcelasPorCantAcampantes();
		int[] arrEsperado = { 6, 0, 0, 0, 0, 0, 0, 0, 0 };

		assertArrayEquals(arrEsperado, arrObtenido);

		camping.ocuparParcelaCon(2, 3);
		camping.ocuparParcelaCon(6, 2);

		int[] arrObtenido2 = camping.contarParcelasPorCantAcampantes();
		int[] arrEsperado2 = { 4, 0, 0, 0, 0, 1, 0, 0, 1 };

		assertArrayEquals(arrEsperado2, arrObtenido2);
	}

	@Test(expected = Error.class)
	public void campingLlenoTest() {
		camping.ocuparParcelaCon(2, 3);
		camping.ocuparParcelaCon(6, 2);
		camping.ocuparParcelaCon(2, 3);
		camping.ocuparParcelaCon(6, 2);
		camping.ocuparParcelaCon(2, 3);
		camping.ocuparParcelaCon(2, 3);
		camping.ocuparParcelaCon(2, 3);
	}

	@Test(expected = Error.class)
	public void superarMaximoAcampantesTest() {
		camping.ocuparParcelaCon(7, 6);
	}

	@Test(expected = Error.class)
	public void minimoDeMayoresTest() {
		camping.ocuparParcelaCon(0, 4);
	}

	@Test(expected = Error.class)
	public void cantidadesNegativasTest() {
		camping.ocuparParcelaCon(2, -4);
	}

	@Test
	public void toStringTest() {
		camping.ocuparParcelaCon(2, 3);
		camping.ocuparParcelaCon(6, 2);
		camping.ocuparParcelaCon(2, 3);
		camping.ocuparParcelaCon(6, 2);
		System.out.println(camping);
	}
}
