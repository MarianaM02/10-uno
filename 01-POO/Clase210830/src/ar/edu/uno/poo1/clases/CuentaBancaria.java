package ar.edu.uno.poo1.clases;

// [acceso] class [NombreClase] {
// NombreClase = sustantivo en singular UpperCamelCase
public class CuentaBancaria {
	// Cuerpo de la clase
	// Clase -> Estructura a partir de la que se puede crear (instanciar) objetos
	// Define, mediante sus atributos, las características que tienen los objetos
	// Define, mediante sus metodos, el comportamiento que tienen los objetos (a q
	// mensajes los objetos saben responder?)
	public static void main(String[] args) {
		// Declaramos dato de tipo CuentaBancaria
		CuentaBancaria cb;
		// Creamos el objeto (se instancia) y se asigna a la variable cb
		cb = new CuentaBancaria();

		CuentaBancaria cb2 = new CuentaBancaria();
		
		System.out.println(cb.toString());
		System.out.println(cb2.toString());

	}
}
