package ar.edu.uno.poo1.clases;

public class Trabajador {
	private static final double BONO_ANTIGUEDAD = 475;
	private int numeroLegajo;
	private double salarioBasico;
	private int antiguedad;

	public Trabajador(int numeroLegajo, double salarioBasico, int antiguedad) {
		this.numeroLegajo = numeroLegajo;
		this.salarioBasico = salarioBasico;
		this.antiguedad = antiguedad;
	}

	public int obtenerNumeroLegajo() {
		return numeroLegajo;
	}

	public void cambiarNumeroLegajo(int numeroLegajo) {
		this.numeroLegajo = numeroLegajo;
	}

	public double obtenerSalarioBasico() {
		return salarioBasico;
	}

	public void cambiarSalarioBasico(double salarioBasico) {
		this.salarioBasico = salarioBasico;
	}

	public int obtenerAntiguedad() {
		return antiguedad;
	}

	public void cambiarAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}

	public double calcularSueldoNeto() {
		return salarioBasico + antiguedad * BONO_ANTIGUEDAD;
	}
}
