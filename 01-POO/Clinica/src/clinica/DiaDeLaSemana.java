package clinica;

/*
 * Una enumeraci�n (o tipo enumerado) representa una lista
 * cuyo contenido por lo general se limita, pero no
 * estrictamente, a constantes.
 * 
 * Son �tiles cuando conocemos a priori el universo de
 * valores que una variable podr� tomar
 */
public enum DiaDeLaSemana {
	LUNES("Lunes"),
	MARTES("Martes"),
	MIERCOLES("Mi�rcoles"),
	JUEVES("Jueves"),
	VIERNES("Viernes"),
	SABADO("S�bado"),
	DOMINGO("Domingo");
	
	private String diaElegido;
	
	private DiaDeLaSemana(String dia) {
		this.diaElegido = dia;
	}
	
	public String getDiaElegido() {
		return this.diaElegido;
	}
}