package clinica;

public enum Especialidad {
	MEDICO_CLINICO("M�dico Cl�nico"), 
	ODONTOLOGO("Odont�logo"), 
	OFTALMOLOGO("Oftalm�logo");

	private String especialidad;

	private Especialidad(String s) {
		this.especialidad = s;
	}

	public String getEspecialidad() {
		return this.especialidad;
	}
}
