package clinica;

public enum Estado {
	ACTIVO("Activo"), 
	BAJA("Dado de Baja"), 
	CUMPLIDO("Cumplido");

	private String estado;
	
	private Estado(String s) {
		this.estado = s;
	}
	
	public String getEstado() {
		return this.estado;
	}
}