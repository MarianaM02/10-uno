package clinica;

public class TurnoMedico {
	private Especialidad especialidad;
	private Estado estado;
	private String profesional;
	private String paciente;
	private DiaDeLaSemana dia;
	
	/**
	 * post: Inicializa con nombre del profesional, su especialidad, 
	 * 				nombre del paciente y el dia de la semana del turno.
	 *
	 * @param especialidad especialidad del profesional
	 * @param profesional nombre del profesional
	 * @param paciente nombre del paciente
	 * @param dia dia de la semana del turno
	 */
	public TurnoMedico(Especialidad especialidad, String profesional, 
						String paciente, DiaDeLaSemana dia) {
		this.especialidad = especialidad;
		this.profesional = profesional;
		this.paciente = paciente;
		this.dia = dia;
		this.estado = Estado.ACTIVO;
	}

	/**
	 * post: Devuelve el estado del turno
	 */
	public Estado getEstado() {
		return estado;
	}

	/**
	 * post: Cambia el estado del turno
	 *
	 * @param estado nuevo estado del turno
	 */
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	/**
	 * post: Devuelve booleano segun si el turno esta activo o no
	 */
	public boolean estaActivo() {
		return this.getEstado() == Estado.ACTIVO;
	}

	/**
	 * pre: Turno est� activo
	 * post: Impresion del turno detallando todos sus datos
	 */
	public String resumenTurno() {
		if (this.estaActivo()) {
			String s = this.paciente + " - " + this.profesional + " - " + this.especialidad.getEspecialidad() + " - "
					+ this.dia.getDiaElegido();
			return s;
		}
		return "El turno est� " + this.estado.getEstado();
	}

}
