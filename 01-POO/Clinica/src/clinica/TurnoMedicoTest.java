package clinica;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TurnoMedicoTest {
	TurnoMedico tm;
	@Before
	public void setUp() throws Exception {
		 tm = new TurnoMedico(Especialidad.MEDICO_CLINICO, "Dr. Pepe", 
				 				"Lucas Carnero", DiaDeLaSemana.LUNES);
	}

	@Test
	public void crearTurnoTest() {
		assertNotNull(tm);
	}
	
	@Test
	public void turnoActivoTest() {
		assertTrue(tm.estaActivo());
	}
	
	@Test
	public void imprimirTurnoActivoTest() {
		String esperado = "Lucas Carnero - Dr. Pepe - M�dico Cl�nico - Lunes";
		String obtenido = tm.resumenTurno();
		assertEquals(esperado, obtenido);
	}
	
	@Test
	public void imprimirTurnoBajaTest() {
		tm.setEstado(Estado.BAJA);
		String esperado = "El turno est� Dado de Baja";
		String obtenido = tm.resumenTurno();
		assertEquals(esperado, obtenido);
	}
	
	@Test
	public void imprimirTurnoCumplidoTest() {
		tm.setEstado(Estado.CUMPLIDO);
		String esperado = "El turno est� Cumplido";
		String obtenido = tm.resumenTurno();
		assertEquals(esperado, obtenido);
	}
	
}
