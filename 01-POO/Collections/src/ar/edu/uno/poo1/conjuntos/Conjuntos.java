package ar.edu.uno.poo1.conjuntos;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Conjuntos {
	
	public static void main(String[] args) {
		
		Set<Integer> conjunto = new HashSet<Integer>();
		conjunto.add(7);	// true
		conjunto.add(3);	// true
		conjunto.add(4);	// true
		conjunto.add(3);	// false
		System.out.println(conjunto);
		conjunto.removeIf(n -> n%2 == 0);
		
		System.out.println(conjunto);
		
		Set<Punto> puntos = new TreeSet<Punto>();
		
		puntos.add(new Punto(1.0, -1.0));
		puntos.add(new Punto(-1.0, -1.0));
		puntos.add(new Punto(-1.0, 1.0));
		puntos.add(new Punto(1.0, 1.0));
		
		System.out.println(puntos);
		
	}

}
