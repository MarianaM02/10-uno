package ar.edu.uno.poo1.conjuntos;

import static java.lang.Math.*;

import java.util.Objects;

public class Punto implements Comparable<Punto>{
	private Double x;
	private Double y;

	public Punto(Double x, Double y) {
		this.x = x;
		this.y = y;
	}

	public Double obtenerX() {
		return x;
	}

	public void cambiarX(Double x) {
		this.x = x;
	}

	public Double obtenerY() {
		return y;
	}

	public void cambiarY(Double y) {
		this.y = y;
	}

	public boolean estaSobreEjeY() {
		return x.compareTo(0.0) == 0;
	}

	public boolean estaSobreEjeX() {
		return y.compareTo(0.0) == 0;
	}

	public boolean esCoordenadaOrigen() {
		return (estaSobreEjeX() && estaSobreEjeY());
	}

	public Double distanciaA(Punto p) {
		double diferenciaEnX = this.obtenerX() - p.obtenerX();
		double diferenciaEnY = this.obtenerY() - p.obtenerY();
		return sqrt(pow(diferenciaEnX, 2) + pow(diferenciaEnY, 2));
	}
	
	public Double distanciaAlOrigen() {
		return distanciaA(new Punto(0.0,0.0));
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Punto other = (Punto) obj;
		return Double.doubleToLongBits(x) == Double.doubleToLongBits(other.x)
				&& Double.doubleToLongBits(y) == Double.doubleToLongBits(other.y);
	}

	@Override
	public String toString() {
		return "[x=" + x + ", y=" + y + "]";
	}

	@Override
	public int compareTo(Punto o) {
		// esta mal, CORREGIR
		return Double.compare(this.distanciaAlOrigen(), o.distanciaAlOrigen());
	}
	
	

}
