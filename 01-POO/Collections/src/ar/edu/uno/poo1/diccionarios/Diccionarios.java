package ar.edu.uno.poo1.diccionarios;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Diccionarios {

	public static void main(String[] args) {
		Map<Integer, Producto> productos = new TreeMap<Integer, Producto>();

		Producto yerba = new Producto("yerba", 3543727, 40);
		Producto arroz = new Producto("arroz", 6496519, 82);
		Producto te = new Producto("te", 8643858, 40);
		Producto fideos = new Producto("fideos", 2789654, 23);

		productos.put(yerba.getCodigo(), yerba);
		productos.put(arroz.getCodigo(), arroz);
		productos.put(te.getCodigo(), te);
		productos.put(fideos.getCodigo(), fideos);

		// iterar un map
		for (Map.Entry<Integer, Producto> p : productos.entrySet()) {
			System.out.println(p);
		}

		Map<Integer, List<Producto>> productosPorStk = new TreeMap<Integer, List<Producto>>();

		for (Map.Entry<Integer, Producto> p : productos.entrySet()) {
			Producto prod = p.getValue();
			// int stk = p.getValue().getStock();
			int stk = prod.getStock();

			List<Producto> listaAux;

			/*if (!productosPorStk.containsKey(stk)) {
				listaAux = new ArrayList<Producto>();
							
			} else {
				listaAux = productosPorStk.get(stk);
			}*/
			
			listaAux = productosPorStk.getOrDefault(stk, new ArrayList<Producto>());
			listaAux.add(prod);	
			productosPorStk.put(stk, listaAux);
		}

		for (Map.Entry<Integer, List<Producto>> p : productosPorStk.entrySet()) {
			System.out.println(p);
		}
		
	}
}
