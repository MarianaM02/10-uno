package ar.edu.uno.poo1.iteradores;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class IteradorMain {
	public static void main(String[] args) {
		List<Integer> numeros = new ArrayList<Integer>();

		numeros.add(3);
		numeros.add(4);
		numeros.add(48);
		numeros.add(11);
		numeros.add(14);
		numeros.add(8);
		numeros.add(34);
		numeros.add(23);
		numeros.add(48);
		numeros.add(48);
		numeros.add(25);
		numeros.add(20);
		numeros.add(48);
		numeros.add(11);
		numeros.add(14);

		// for
		for (int k = 0; k < numeros.size(); k++) {
			System.out.println("for: " + numeros.get(k));
		}

		System.out.println();

		// for-each
		for (Integer numero : numeros) {
			System.out.println("for-each: " + numero);
		}

// 		PROBLEMA: De la lista de "numeros", remover los números pares
//		Funciona a medias: dentro del cuerpo del for, cambia el tamaño de la colección
//		for(int k = 0; k < numeros.size(); k++) {
//			if (numeros.get(k) % 2 == 0) {
//				numeros.remove(k);
//			}
//		}

//		No funciona. ConcurrentModificationException
//		for(Integer numero : numeros) {
//			if (numero % 2 == 0) {
//				numeros.remove(numero);
//			}
//		}
		System.out.println();

		/*
		 * Iterator<Integer> iterador = numeros.iterator(); while(iterador.hasNext()) {
		 * if(iterador.next() % 2 == 0) { iterador.remove(); } }
		 */
		System.out.println(Collections.frequency(numeros, 48));
	
		
		numeros.removeIf(n -> n % 2 != 0);
		System.out.println(Collections.frequency(numeros, 48));

		System.out.println(numeros);
	}
}
