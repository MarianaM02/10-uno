package ar.edu.uno.poo1.iteradores;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class IteradorMapaConListaMain {
	public static void main(String[] args) {
		// Crear un mapa donde la clave sea UNA letra
		// y el valor una lista de palabras que empiezan con dicha letra
		Map<Character, List<String>> palabras = new TreeMap<Character, List<String>>();

		List<String> palabrasConC = new ArrayList<String>();
		palabrasConC.add("Casa");
		palabrasConC.add("Correr");
		
		List<String> palabrasConA = new ArrayList<String>();
		palabrasConA.add("Auto");
		palabrasConA.add("Avi�n");
		
		palabras.put('C', palabrasConC);
		palabras.put('A', palabrasConA);
		
		// Creo un iterador para recorrer el mapa de palabras
		Iterator<Map.Entry<Character, List<String>>> iterador = palabras.entrySet().iterator();
		
		while(iterador.hasNext()) {
			Map.Entry<Character, List<String>> actual = iterador.next();
			System.out.println(actual.getKey() + ": " );
			actual.getValue().forEach((s) -> System.out.println(s));
		}		
	}
}
