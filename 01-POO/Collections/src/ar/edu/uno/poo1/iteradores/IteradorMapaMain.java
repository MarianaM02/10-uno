package ar.edu.uno.poo1.iteradores;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class IteradorMapaMain {
	public static void main(String[] args) {
		// Mapa donde la clave sea el número de DNI
		// de una persona y el valor sea su nombre
		Map<Integer, String> personas = new TreeMap<Integer, String>();
		
		personas.put(1, "Lucas");
		personas.put(2, "Marcelo");
		personas.put(3, "Walter");
		personas.put(4, "Héctor");
		
		// ¿Podemos usar iteradores para recorrer un mapa?
		for(Map.Entry<Integer, String> persona : personas.entrySet()) {
			System.out.println("DNI: " + persona.getKey() + " : Nombre: " + persona.getValue());
		}
		
		System.out.println();
		
		Iterator<Map.Entry<Integer, String>> it = personas.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<Integer, String> actual = it.next();			
			System.out.println("DNI: " + actual.getKey() + " | Nombre: " + actual.getValue());
		}
		
	}
}
