package ar.edu.uno.poo1.lists;

import java.util.Comparator;

public class ApellidoComparator implements Comparator<Persona> {

	@Override
	public int compare(Persona arg0, Persona arg1) {
		return arg0.getApellido().compareTo(arg1.getApellido());
	}

}
