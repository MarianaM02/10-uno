package ar.edu.uno.poo1.lists;

import java.util.Comparator;

public class EdadComparator implements Comparator<Persona> {

	@Override
	public int compare(Persona arg0, Persona arg1) {
		return Integer.compare(arg0.getEdad(), arg1.getEdad());
	}

}
