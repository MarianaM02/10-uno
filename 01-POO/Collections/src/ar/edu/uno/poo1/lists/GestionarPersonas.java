package ar.edu.uno.poo1.lists;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class GestionarPersonas {

	public static List<Persona> getPersona(String archivo) {
		List<Persona> personas = new LinkedList<Persona>();
		Scanner sc = null;

		try {
			sc = new Scanner(new File(archivo));

			while (sc.hasNext()) {
				// leo cada linea del archivo
				String linea = sc.nextLine();
				String datos[] = linea.split(" ");

				// creo una persona a partir de los datos leidos en cada linea
				String dni = datos[0];
				String apellido = datos[1];
				int edad = Integer.parseInt(datos[2]);
				Persona p = new Persona(dni, apellido, edad);

				// agrego la persona a la lista, siempre y cuando no este repetida
				if (!personas.contains(p)) {
					personas.add(p);
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		sc.close();
		return personas;
	}
	
	public static void ordenarPersonasPorDNI(List<Persona> lista) {
		Collections.sort(lista, new DniComparator());
	}
	
	public static void ordenarPersonasPorApellido(List<Persona> lista) {
		Collections.sort(lista, new ApellidoComparator());
	}
	
	public static void ordenarPersonasPorEdad(List<Persona> lista) {
		Collections.sort(lista, new EdadComparator());
	}
	
	public static List<Persona> getPersonasMayoresDeEdad(List<Persona> personas, Integer edad) {
		List<Persona> personasMayores = new ArrayList<Persona>();
		for(Persona p : personas) {
			if (p.getEdad() > edad) {
				personasMayores.add(p);
			}
		}
		return personasMayores;
	}
	
	public static void escribirPersonas(List<Persona> personas, String file) throws IOException {
		PrintWriter salida = new PrintWriter(new FileWriter(file));
		for (Persona p : personas) {
			salida.println(p);
		}
		salida.close();
	}
	
	public static void escribirMayoresDeEdadOrdenadoPorDNI(List<Persona> personas, int edad) throws IOException {
		
		List<Persona> personasMayores = getPersonasMayoresDeEdad(personas, edad);
		ordenarPersonasPorDNI(personasMayores);
		escribirPersonas(personasMayores, "MayoresDe"+edad+"OrdenadosPorDNI.csv");
		
	}
	
public static void escribirMayoresDeEdadOrdenadoPorEdad(List<Persona> personas, int edad) throws IOException {
		
		List<Persona> personasMayores = getPersonasMayoresDeEdad(personas, edad);
		ordenarPersonasPorEdad(personasMayores);
		escribirPersonas(personasMayores, "MayoresDe"+edad+"OrdenadosPorEdad.csv");
		
	}
}
