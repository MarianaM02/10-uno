package ar.edu.uno.poo1.lists;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Persona {

	private String dni;
	private String apellido;
	private int edad;

	public Persona(String dni, String nombre, int edad) {
		super();
		this.dni = dni;
		this.apellido = nombre;
		this.edad = edad;
	}

	public String getDni() {
		return dni;
	}

	public String getApellido() {
		return apellido;
	}

	public int getEdad() {
		return edad;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apellido == null) ? 0 : apellido.hashCode());
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + edad;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (apellido == null) {
			if (other.apellido != null)
				return false;
		} else if (!apellido.equals(other.apellido))
			return false;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		if (edad != other.edad)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return dni + "," + apellido + "," + edad;
	}
	
	public static void main(String[] args) {
		List<Integer> vector = new ArrayList<Integer>();
		System.out.println("Esta vacio?: " + vector.isEmpty());
		vector.add(2);
		vector.add(5);
		vector.add(3);
		System.out.println("toString: " + vector);
		vector.remove((Integer)2); // por indice
		System.out.println("toString: " + vector);
		System.out.println("Esta vacio?: " + vector.isEmpty());
		System.out.println("Posici�n del elemento 5: " + vector.indexOf(5));
		System.out.println("Tama�o del vector: " + vector.size());
		
		System.out.println(vector.get(0));
		
		
		List<Integer> lista = new LinkedList<Integer>();
		System.out.println("Esta vacia?: " + lista.isEmpty());
		lista.add(2);
		lista.add(0, 5);
		lista.add(3);
		System.out.println("toString: " + lista);
		lista.remove(1);
		System.out.println("toString: " + lista);
		System.out.println("Esta vacia?: " + lista.isEmpty());
		System.out.println("Elemento en pos 1?: " + lista.get(1));
		System.out.println("Tama�o de la lista: " + lista.size());
		
	}
	
	

}
