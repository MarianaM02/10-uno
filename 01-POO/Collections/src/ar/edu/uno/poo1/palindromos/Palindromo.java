package ar.edu.uno.poo1.palindromos;

import java.util.Stack;

public class Palindromo {
	Stack<Character> pila = new Stack<Character>();
	
	public boolean esPalindromo(String original) {
		procesarStr(original);
		String strAlReves = darVuelta();
		
		return original.equals(strAlReves);
	}

	private String darVuelta() {
		String nuevoStr = "";
		Stack<Character> aux = new Stack<Character>();
		while(!pila.empty()) {
			aux.push(pila.pop());
		}
		while(!aux.empty()) {
			nuevoStr += pila.push(aux.pop());
		}
		return nuevoStr;
	}

	private void procesarStr(String s) {
		char[] charArr = s.toCharArray();
		for (char c : charArr) {
			if (Character.isAlphabetic(c)) {
				pila.push(c);
			}
		}
		
	}
	

}
