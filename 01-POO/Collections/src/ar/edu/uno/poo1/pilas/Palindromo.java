package ar.edu.uno.poo1.pilas;

import java.util.Stack;

/*
 * 2) Dada una palabra e imprima un mensaje indicando si es 
 * palindromo o no. Una palabra es palindromo cuando se lee 
 * igual hacia adelante que hacia atras.
 */

public class Palindromo {

	Stack<Character> pila = new Stack<Character>();

	boolean esPalabraPalindromica(String original) {
		ingresarPalabra(original);

		String dadaVuelta = darVuelta();

		return original.equalsIgnoreCase(dadaVuelta);
	}

	private void ingresarPalabra(String str) {
		char[] arr = str.toCharArray();

		for (char c : arr) {
			pila.push(c);
		}
	}

	private String darVuelta() {
		String s = "";

		while (!pila.empty()) {
			s += pila.pop();
		}

		return s;
	}

	public static void main(String[] args) {

		Palindromo p = new Palindromo();
		System.out.println(p.esPalabraPalindromica("oso"));
		System.out.println(p.esPalabraPalindromica("radar"));
		System.out.println(p.esPalabraPalindromica("reconocer"));
		System.out.println(p.esPalabraPalindromica("rotor"));
		System.out.println(p.esPalabraPalindromica("seres"));
		System.out.println(p.esPalabraPalindromica("somos"));
		System.out.println(p.esPalabraPalindromica("polimorfismo"));

	}
}
