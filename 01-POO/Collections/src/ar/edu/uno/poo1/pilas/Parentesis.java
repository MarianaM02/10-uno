package ar.edu.uno.poo1.pilas;

import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedQueue;

/*
 * 3) Dada una cadena compuesta por s�mbolos ( y ), determinar 
 * si est�n correctamente balanceados. Si no lo est�n, mostrar 
 * un mensaje de error.
 */
public class Parentesis {

	Stack<Character> pila = new Stack<Character>();
	Queue<Character> cola = new ConcurrentLinkedQueue<Character>();

	void estaBalanceado(String original) {
		ingresarString(original);

		while (!pila.empty()) {
			if ((cola.poll() == '(') != (pila.pop() == ')')) {
				System.out.println(original + " no est� balanceada");
				return;
			}
		}
		System.out.println(original + " est� balanceada");
	}

	private void ingresarString(String str) {
		pila.clear();
		cola.clear();
		char[] arr = str.toCharArray();

		for (char c : arr) {
			pila.push(c);
			cola.add(c);
		}
	}

	public static void main(String[] args) {

		Parentesis p = new Parentesis();
		p.estaBalanceado("((((())");
		p.estaBalanceado("()");
		p.estaBalanceado("())))");
		p.estaBalanceado("((((()))))");

	}

}
