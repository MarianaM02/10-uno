package ar.edu.uno.poo1.pilas;

import java.util.Stack;

/*
 * 1) Un conductor maneja de un pueblo origen a un pueblo destino, 
 * pasando por varios pueblos. Una vez en el pueblo destino, el 
 * conductor debe regresar a casa por el mismo camino. Mostrar 
 * el camino recorrido tanto de ida como de vuelta.
 */
public class Tren {
	Stack<String> pila = new Stack<String>();

	void ingresarIda(String str, String separador) {
		String[] arr = str.split(separador);

		for (String s : arr) {
			pila.push(s);
		}
	}

	void mostrarRecorridoCompleto() {
		Stack<String> aux = new Stack<String>();

		String ida = "";
		String vuelta = "";

		while (!pila.empty()) {
			vuelta += aux.push(pila.pop()) + " - ";
		}

		while (!aux.empty()) {
			ida += pila.push(aux.pop()) + " - ";
		}

		ida = ida.substring(0, ida.length() - 3);
		vuelta = vuelta.substring(0, vuelta.length() - 3);

		System.out.println(ida);
		System.out.println(vuelta);
	}

	public static void main(String[] args) {

		Tren t = new Tren();
		t.ingresarIda("Liniers - Ciudadela - Ramos Mej�a - Haedo - Castelar", " - ");
		t.mostrarRecorridoCompleto();

	}

}
