package ar.edu.uno.poo1.productos;

import java.util.ArrayList;
import java.util.Collections;

public class Inventario {
	
	private ArrayList<Producto> inventario = new ArrayList<Producto>();
	
	public void agregarProducto(Producto prod) {
		inventario.add(prod);
	}
	
	
	public static void main(String[] args) {
		ArrayList<Integer> lista = new ArrayList<Integer>();
		lista.add(67);
		lista.add(98);
		lista.add(-57);
		lista.add(2);
		lista.add(0);
		System.out.println(lista);
		Collections.sort(lista);
		System.out.println(lista);
	}
	
}
