package ar.edu.uno.poo1.productos;

public class Producto implements Comparable<Producto>{
	
	private int codigo;
	private String descripcion;
	private double importe;
	private int stock;
	
	public Producto(int codigo, String descripcion, double importe, int stock) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.importe = importe;
		this.stock = stock;
	}

	
	
	@Override
	public String toString() {
		return "Producto [codigo=" + codigo + ", descripcion=" + descripcion + ", importe=" + importe + ", stock="
				+ stock + "]\n";
	}



	@Override
	public int compareTo(Producto arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
