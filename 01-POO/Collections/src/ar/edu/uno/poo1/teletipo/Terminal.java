package ar.edu.uno.poo1.teletipo;

import java.util.Stack;

public class Terminal {

	private Stack<Character> pila = new Stack<Character>();
	
	public void procesarTeletipo(String s) {
		
		char[] charArr = s.toCharArray();
		for(char c : charArr) {
			switch (c) {
				case '/':
					if (!pila.empty())
						pila.pop();
					break;
				case '&':
					if (!pila.empty())
						pila.clear();
					break;
				default:
					pila.push(c);
					break;
			}
		}
	}
	
	public String darVueltaPila() {
		String nuevoStr = "";
		Stack<Character> aux = new Stack<Character>();
		while(!pila.empty()) {
			aux.push(pila.pop());
		}
		while(!aux.empty()) {
			nuevoStr += pila.push(aux.pop());
		}
		return nuevoStr;
	}
	
	public static void main(String[] args) {
		Terminal t = new Terminal();
		t.procesarTeletipo("AXR&DF/GHJ/K/W");
		System.out.println(t.darVueltaPila());
	}
}
