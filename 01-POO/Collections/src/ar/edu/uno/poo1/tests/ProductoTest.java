package ar.edu.uno.poo1.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.edu.uno.poo1.productos.Producto;

public class ProductoTest {

	List<Producto> inventario = new ArrayList<Producto>();
	
	@Before
	public void setUp() throws Exception {
		inventario.add(new Producto(00001, "Monitor 21\"", 8000.50, 5));
		inventario.add(new Producto(00001, "Monitor 24\"", 10000.50, 5));
	}

	@Test
	public void crearListaProductosTest() {
		assertNotNull(inventario);
		
		int esperado = 2;
		int obtenido = inventario.size();
		
		assertEquals(esperado, obtenido);
	}
	
	

}
