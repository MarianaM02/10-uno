package ar.edu.uno.poo1.tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ar.edu.uno.poo1.teletipo.Terminal;

public class TerminalTest {
	
	

	@Test
	public void teletipoTest() {
		Terminal t = new Terminal();
		
		String esperado = "ae";
		t.procesarTeletipo("abc/d//e");
		String obtenido = t.darVueltaPila();
		System.out.println(obtenido);
		
		assertEquals(esperado, obtenido);
		
		
		esperado = "DGHW";
		t.procesarTeletipo("AXR&DF/GHJ/K/W");
		obtenido = t.darVueltaPila();
		System.out.println(obtenido);
		
		assertEquals(esperado, obtenido);
	}

}
