package cuentasBancarias;

public class CajaDeAhorro {
	private double saldo;
	private String titular;

	/**
	 * post: la instancia queda asignada al titular indicado y con saldo igual a 0.
	 */
	public CajaDeAhorro(String titularDeLaCuenta) {
		this.saldo = 0;
		this.titular = titularDeLaCuenta;
	}

	/**
	 * post: devuelve el nombre del titular de la Caja de Ahorro.
	 */
	public String obtenerTitular() {
		return this.titular;
	}

	/**
	 * post: devuelve el saldo de la Caja de Ahorro.
	 */
	public double consultarSaldo() {
		return this.saldo;
	}

	/**
	 * pre : monto es un valor mayor a 0.
	 * 
	 * post: aumenta el saldo de la Caja de Ahorro seg�n el monto depositado.
	 */
	public void depositar(double monto) {
		if (monto > 0) {
			this.saldo += monto;
		}
		else {
			throw new Error("Valor inv�lido! (Valor negativo o nulo)");			
		}
	}

	/**
	 * pre : monto es un valor mayor a 0 y menor o igual que el saldo de la Caja de
	 * Ahorro.
	 * 
	 * post : disminuye el saldo de la Caja de Ahorro seg�n el monto extraido.
	 */
	public boolean extraer(double monto) {
		if (monto <= 0 )
			throw new Error("Valor inv�lido! (Valor negativo o nulo)");
		if (monto <= saldo) {
			this.saldo -= monto;
			return true;
		}
		else
			throw new Error("Valor inv�lido! (Saldo Insuficiente)");
	}

}

