package cuentasBancarias;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CajaDeAhorroTest {
	CajaDeAhorro ca;
	
	@Before
	public void setUp() throws Exception {
		ca = new CajaDeAhorro("Mariana");
	}

	@Test
	public void creoCuentaTest() {
		assertNotNull(ca);
	}

	@Test
	public void consultarTitularTest() {
		String esperado = "Mariana";
		String obtenido = ca.obtenerTitular();
		assertEquals(esperado, obtenido);
	}

	@Test
	public void consultarSaldoTest() {
		double esperado = 0;
		double obtenido = ca.consultarSaldo();
		// Al comparar numeros, el 3er valor declara la precision (tolerancia)
		assertEquals(esperado, obtenido, 0.001);
	}

	@Test
	public void depositarTest() {
		ca.depositar(1000);
		double esperado = 1000;
		double obtenido = ca.consultarSaldo();
		assertEquals(esperado, obtenido, 0.001);
	}

	@Test(expected = Error.class)
	public void depositarNegativoTest() {
		ca.depositar(-10);
	}

	@Test(expected = Error.class)
	public void depositarCeroTest() {
		ca.depositar(0);
	}

	@Test
	public void extraerTest() {
		ca.depositar(1000);
		ca.extraer(500.5);
		double esperado = 499.5;
		double obtenido = ca.consultarSaldo();
		assertEquals(esperado, obtenido, 0.001);
	}
	
	// Chequear
	@Test(expected = Error.class)
	public void extraerMuchoTest() {
		ca.depositar(1000);
		ca.extraer(1003);
		double esperado = 1000;
		double obtenido = ca.consultarSaldo();
		assertEquals(esperado, obtenido, 0.001);
		assertFalse(ca.extraer(1500));
	}

	@Test(expected = Error.class)
	public void extraerNegativoTest() {
		ca.depositar(1000);
		ca.extraer(-300);
	}
}
