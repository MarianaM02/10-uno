package ar.edu.uno.poo1.banco;

import java.util.ArrayList;

public class Banco {
	private ArrayList<Cuenta> cuentas;
	
	public Banco(int cantidadCuentas) {
		this.cuentas = new ArrayList<Cuenta>(cantidadCuentas);
	}
	
	public void totalSaldoMaxPenalizacion() {
		double total = 0;
		int penalizacionMax = 0;
		
		for(Cuenta cta : cuentas) {
			if (cta instanceof CuentaDeAhorroEsp){
				CuentaDeAhorroEsp cae = (CuentaDeAhorroEsp) cta;
				if (cae.getPenalizacion() > penalizacionMax) {
					penalizacionMax = cae.getPenalizacion();
				}
			}
			total += cta.getSaldo();
		}
		
		System.out.println("Total = $" + total +
				"\nMax Penalizacion = " + penalizacionMax +"%");
		
	}

	public void agregarCuenta(Cuenta c) {
		this.cuentas.add(c);
	}

	@Override
	public String toString() {
		return "Banco [" + cuentas + "]";
	}
	
	
}
