package ar.edu.uno.poo1.banco;

public abstract class Cuenta {
	
	private String dniTitular;
	private double saldo;

	public Cuenta(String dniTitular, double saldo) {
		super();
		this.dniTitular = dniTitular;
		this.saldo = saldo;
	}

	protected void ingresarDinero(double monto) {
		this.saldo += monto;
	}

	protected abstract void retirarDinero(double monto) throws SaldoNegativoException;

	protected String getDniTitular() {
		return this.dniTitular;
	}

	protected double getSaldo() {
		return this.saldo;
	}
	
	protected void setDniTitular(String dniTitular) {
		this.dniTitular = dniTitular;
	}

	protected void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	@Override
	public String toString() {
		return "\n\nDNI Titular: " + dniTitular + "\nSaldo: $" + saldo;
	}

}
