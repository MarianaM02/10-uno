package ar.edu.uno.poo1.banco;

public class CuentaCorriente extends Cuenta {

	private CuentaDeAhorro cuentaDeAhorroAsociada;

	public CuentaCorriente(String dniTitular, double saldo, CuentaDeAhorro cuenta) {
		super(dniTitular, saldo);
		this.cuentaDeAhorroAsociada = cuenta;
	}


	@Override
	protected void retirarDinero(double monto) throws SaldoNegativoException {
		if (this.getSaldo() < monto) {
			cuentaDeAhorroAsociada.retirarDinero(monto - getSaldo());
			this.setSaldo(0);
		} else {
			this.setSaldo(this.getSaldo() - monto);
		}
		
	}


	@Override
	public String toString() {
		return super.toString() + "\nTitular Cuenta Asociada: " + cuentaDeAhorroAsociada.getDniTitular();
	}
		
	

}
