package ar.edu.uno.poo1.banco;

public class CuentaDeAhorro extends Cuenta {
	
	public CuentaDeAhorro(String dniTitular, double saldo) {
		super(dniTitular, saldo);
	}

	@Override
	protected void retirarDinero(double monto) throws SaldoNegativoException {
		if (this.getSaldo() < monto) {
			throw new SaldoNegativoException("Saldo Insuficiente");
		}
		this.setSaldo(this.getSaldo() - monto);
	}

}
