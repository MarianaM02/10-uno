package ar.edu.uno.poo1.banco;

public class CuentaDeAhorroEsp extends CuentaDeAhorro {
	
	private int penalizacion;

	public CuentaDeAhorroEsp(String dniTitular, double saldo, int penalizacion) {
		super(dniTitular, saldo);
		this.penalizacion = penalizacion;
	}
	
	public int getPenalizacion() {
		return penalizacion;
	}

	@Override
	protected void retirarDinero(double monto) throws SaldoNegativoException {
		super.retirarDinero(monto + monto * penalizacion / 100);
	}

	@Override
	public String toString() {
		return super.toString() + "\nPenalizacion: " + penalizacion;
	}
	
	

}
