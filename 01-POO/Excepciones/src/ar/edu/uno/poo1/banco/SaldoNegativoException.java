package ar.edu.uno.poo1.banco;

@SuppressWarnings("serial")
public class SaldoNegativoException extends Exception {

	public SaldoNegativoException(String message) {
		super(message);
	}
	
}
