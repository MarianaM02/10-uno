package ar.edu.uno.poo1.excepciones;

public class EdadNoValidaException extends Exception {

	public EdadNoValidaException(String mensaje) {
		super(mensaje);
	}
	
	public void mostrarMensaje() {
		System.out.println(super.getMessage());
	}
		
}
