package ar.edu.uno.poo1.excepciones;

public class Punto1 {
	/*
	 * 1) Cree una clase con un m�todo main() que genere un objeto de la clase
	 * Exception dentro de un bloque try. Proporcione al constructor de Exception un
	 * argumento String. Capture la excepci�n dentro de una cl�usula catch e imprima
	 * el argumento String. A�ada una clausula finally e imprima un mensaje para
	 * demostrar que pas� por all�
	 */

	public static void main(String[] args) {
		try {
			throw new Exception("Este es el mensaje que describe la excepci�n.");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Por finally se ha pasado.");
		}
	}
}
