package ar.edu.uno.poo1.excepciones;

public class Punto2 {
	/*
	 * 2) Defina una referencia a un objeto e inicializela a null. Trate de invocar
	 * un m�todo a trav�s de esta referencia. Ahora rodee el c�digo con una clausula
	 * try-catch para probar la nueva excepci�n.
	 */

	public static void main(String[] args) {
		try {
			//Defino referencia nula
			String s = null;
			// Implicitamente se lanza la excepcion
			System.out.println(s.length());
		} catch (NullPointerException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
}
