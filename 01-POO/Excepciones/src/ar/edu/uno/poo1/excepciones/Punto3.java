package ar.edu.uno.poo1.excepciones;

public class Punto3 {
	public static void main(String[] args) {
// Declaro arreglo de 3 enteros
		Integer[] numeros = new Integer[3];

		numeros[0] = 3;
		numeros[1] = -1;
		numeros[2] = 32;

		try {
			mostrarPosicionDelArreglo(numeros, 0);
			mostrarPosicionDelArreglo(numeros, 4);
		} catch (ArrayIndexOutOfBoundsException ex) {
			// Muestro el mensaje que acompa�a a la excepcion
			System.out.println(ex.getMessage());
			// Muestro la pila de llamadas (stack-trace)
			ex.printStackTrace();
		} finally {
			// Este bloque es opcional y se usa (por lo gral) para liberar recursos, ie
			// cerrar archivos, conexiones

			System.out.println("Este mensaje lo vas a ver siempre");
		}

	}

	static void mostrarPosicionDelArreglo(Integer[] arr, int pos) {
		System.out.println(arr[pos]);
	}
}
