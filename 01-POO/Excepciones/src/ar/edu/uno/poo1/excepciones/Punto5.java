package ar.edu.uno.poo1.excepciones;

import java.util.Scanner;

public class Punto5 {
	/**
	 * 5) Defina un comportamiento de tipo reanudaci�n utilizando un bucle while que
	 * se repita hasta que se deje de generar una excepci�n.
	 */
	public static void main(String[] args) {
		// Comportamiento de tipo reanudaci�n:
		Boolean continuar = Boolean.TRUE;
		while (continuar) {
			try {
				System.out.print("Introduce un n�mero entero: ");
				Scanner scanner = new Scanner(System.in);
				Integer numero = scanner.nextInt();
				scanner.close();
				System.out.println("El cuadrado de " + numero + " = " + (numero * numero));
				continuar = Boolean.FALSE;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
