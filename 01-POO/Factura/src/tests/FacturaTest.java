package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ventas.Factura;
import ventas.Producto;

public class FacturaTest {
	
	Factura fact;
	Producto p1, p2, p3, p4, p5;

	@Before
	public void setUp() throws Exception {
		fact = new Factura();
		p1 = new Producto(1235742452, "Monitor 22\"", 20000.0);
		p2 = new Producto(1236427858, "Toallon", 1355.0);
		p3 = new Producto(1231361757, "Rompecabezas", 2183.0);
		p4 = new Producto(1231634578, "Jab�n de tocador", 107.0);
		p5 = new Producto(1231614668, "Queso fresco", 713.5);
		Producto[] p = new Producto[0]; 
		
	}

	@Test
	public void crearFacturaYProductoTest() {
		assertNotNull(fact);
		assertNotNull(p1);
	}
	
	@Test
	public void agregarProductosYBuscarIndiceTest() {
		fact.agregarProducto(p1);
		Producto obtenido = fact.retornarProductoEnLaPosicion(0);
		assertEquals(p1, obtenido);
		
		fact.agregarProducto(p4);
		fact.agregarProducto(p3);
		obtenido = fact.retornarProductoEnLaPosicion(2);
		assertEquals(p3, obtenido);
	}
	
	@Test
	public void calcularPromedioTest() {
		fact.agregarProducto(p1);
		fact.agregarProducto(p4);
		fact.agregarProducto(p3);
		
		double obtenido = fact.calcularPrecioPromedio();
		double esperado = 7430;
		assertEquals(esperado, obtenido, 0.0001);
	}
	
	@Test
	public void productosMasCarosTest() {
		fact.agregarProducto(p1);
		fact.agregarProducto(p2);
		fact.agregarProducto(p3);
		fact.agregarProducto(p4);
		fact.agregarProducto(p5);
		ArrayList<Producto> arr = fact.productosMasCaros(1000);
		
		assertEquals(arr.size(), 3);
		assertTrue(arr.get(0).equals(p1));
		assertTrue(arr.get(1).equals(p2));
		assertTrue(arr.get(2).equals(p3));
		
	}
	
	@Test
	public void calcuarTotalTest() {
		fact.agregarProducto(p4);
		fact.agregarProducto(p2);
		fact.agregarProducto(p3);
		fact.agregarProducto(p4);
		fact.agregarProducto(p5);
		
		double obtenido = fact.calcularTotal();
		double esperado = 4465.5;
		assertEquals(esperado, obtenido, 0.0001);
	}
	
	@Test (expected = Error.class)
	public void posicionInvalidaTest() {
		fact.agregarProducto(p1);
		fact.agregarProducto(p2);
		
		fact.retornarProductoEnLaPosicion(8);
	}
	
	@Test (expected = Error.class)
	public void productoNuloTest() {
		fact.agregarProducto(p1);
		fact.agregarProducto(null);
		
		fact.retornarProductoEnLaPosicion(1);
	}
	
	@Test (expected = Error.class)
	public void agregarDemasiadosProductosTest() {
		fact.agregarProducto(p1);
		fact.agregarProducto(p2);
		fact.agregarProducto(p3);
		fact.agregarProducto(p4);
		fact.agregarProducto(p5);
		fact.agregarProducto(p1);
		fact.agregarProducto(p2);
		fact.agregarProducto(p3);
		fact.agregarProducto(p4);
		fact.agregarProducto(p5);
		fact.agregarProducto(p1);
		fact.agregarProducto(p2);
		fact.agregarProducto(p3);
		fact.agregarProducto(p4);
		fact.agregarProducto(p5);
		fact.agregarProducto(p1);
		fact.agregarProducto(p2);
		fact.agregarProducto(p3);
		fact.agregarProducto(p4);
		fact.agregarProducto(p5);
		fact.agregarProducto(p1);
		fact.agregarProducto(p2);
		fact.agregarProducto(p3);
		fact.agregarProducto(p4);
		fact.agregarProducto(p5);
		fact.agregarProducto(p1);
		fact.agregarProducto(p2);
		fact.agregarProducto(p3);
		fact.agregarProducto(p4);
		fact.agregarProducto(p5);
	}

	@Test 
	public void mostrarFacturaTest() {
		fact.agregarProducto(p4);
		fact.agregarProducto(p2);
		fact.agregarProducto(p3);
		fact.agregarProducto(p4);
		fact.agregarProducto(p5);
	
		System.out.println(fact);
	}
	
}