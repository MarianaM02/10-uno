package ventas;

import java.util.ArrayList;

public class Factura {

	private static final int MAX_ITEMS = 25;
	private ArrayList<Producto> productos = new ArrayList<Producto>(MAX_ITEMS);

	/**
	 * post: Calcula el precio promedio de todos los productos que formen parte de
	 * la factura y lo devuelve.
	 */
	public double calcularPrecioPromedio() {
		return calcularTotal() / productos.size();
	}

	/**
	 * post: Recibe por par�metro un precio unitario m�nimo, y retorna un arreglo de
	 * productos tal que el precio unitario de dichos productos sea mayor o igual
	 * que el recibido como argumento.
	 */
	public ArrayList<Producto> productosMasCaros(double precio) {
		ArrayList<Producto> arr = new ArrayList<Producto>(MAX_ITEMS);
		for (Producto p : productos) {
			if (p.getPrecioUnitario() >= precio)
				arr.add(p);
		}
		arr.trimToSize();
		return arr;
	}

	/**
	 * pre: Posicion sea numero v�lido.
	 * 
	 * post: Retorna el producto hallado en la posici�n indicada por par�metro.
	 */
	public Producto retornarProductoEnLaPosicion(int posicion) {
		if (posicion >= productos.size() || posicion < 0)
			throw new Error("VALOR INVALIDO: posicion fuera de rango");
		else if (productos.get(posicion) != null)
			return productos.get(posicion);
		else
			throw new Error("SIN PRODUCTO: No hay producto en esa posicion.");
	}

	/**
	 * post: Retorna el precio total a abonar por la factura.
	 */
	public double calcularTotal() {
		double total = 0;
		for (Producto p : productos) {
			total += p.getPrecioUnitario();
		}
		return total;
	}

	/**
	 * pre: Cantidad de productos maxima en lista
	 * 
	 * post: Agrega un producto a la factura
	 */
	public void agregarProducto(Producto producto) {
		if (productos.size() == MAX_ITEMS)
			throw new Error("LISTA LLENA: No se puede agregar mas productos.");
		else
			productos.add(producto);
	}

	@Override
	public String toString() {
		return "Factura [productos=" + productos + "]";
	}
}
