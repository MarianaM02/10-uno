package ventas;

public class Producto {
	private long sku;
	private String nombre;
	private double precioUnitario;
	
	public Producto(long sku, String nombre, double precioUnitario) {
		super();
		this.sku = sku;
		this.nombre = nombre;
		this.precioUnitario = precioUnitario;
	}

	public long getSku() {
		return sku;
	}

	public String getNombre() {
		return nombre;
	}

	public double getPrecioUnitario() {
		return precioUnitario;
	}

	@Override
	public String toString() {
		return "\nProducto [sku=" + sku + " " + nombre + " $" + precioUnitario + "]";
	}

	
}
