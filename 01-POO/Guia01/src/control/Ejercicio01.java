package control;

public class Ejercicio01 {
	//1. Leer dos valores num�ricos enteros e indicar cual 
	//es el mayor y cual es el menor. Considerar que ambos 
	//valores son diferentes.
	
	public static void main(String[] args) {
		int valorA=87;
		int valorB=21;
		int menor, mayor;
		
		if (valorA > valorB) {
			menor = valorB;
			mayor = valorA;
		} else {
			mayor = valorB;
			menor = valorA;
		}
		
		System.out.println(menor);
		System.out.println(mayor);
	}

}
