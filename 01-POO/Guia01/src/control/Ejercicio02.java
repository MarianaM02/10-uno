package control;

public class Ejercicio02 {
	//2. Leer tres valores numéricos enteros, indicar cual es el 
	//mayor, cuál es el del medio y cuál el menor. Considerar que 
	//los tres valores son diferentes.
	
	public static void main(String[] args) {
		int valorA=87;
		int valorB=54;
		int valorC=23;

		ordenarTresNumeros(valorA, valorB, valorC);
		ordenarTresNumeros(11, 22, 33);
		ordenarTresNumeros(22, 33, 11);
		ordenarTresNumeros(33, 11, 22);
		ordenarTresNumeros(11, 33, 22);
		ordenarTresNumeros(22, 11, 33);
		ordenarTresNumeros(33, 22, 11);

	}

	private static void ordenarTresNumeros(int valorA, int valorB, int valorC) {
		// C>B>A
		// C>A>B
		// A>B>C
		// A>C>B
		// B>A>C
		// B>C>A
		
		int menor=0, medio=0, mayor=0;
		if (valorA < valorB && valorA < valorC) {
			menor = valorA;
			if (valorB < valorC) {
				medio = valorB;
				mayor = valorC;
			} else {
				medio = valorC;
				mayor = valorB;
			}
		}
		
		if (valorB < valorA && valorB < valorC) {
			menor = valorB;
			if (valorA < valorC) {
				medio = valorA;
				mayor = valorC;
			} else {
				medio = valorC;
				mayor = valorA;
			}
		}
		
		if (valorC < valorB && valorC < valorA) {
			menor = valorC;
			if (valorB < valorA) {
				medio = valorB;
				mayor = valorA;
			} else {
				medio = valorA;
				mayor = valorB;
			}
		}
		
		System.out.println(menor);
		System.out.println(medio);
		System.out.println(mayor);
		System.out.println("");

	}
}
