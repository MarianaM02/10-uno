package control;
/*3. Leer un valor numérico que representa un día 
 * de la semana. Se pide mostrar por pantalla el nombre 
 * del día considerando que el lunes es el día 1, el 
 * martes es el día 2 y así, sucesivamente. 
 */
public class Ejercicio03 {
	public static void main(String[] args) {
		
		dia(3);
	}

	private static void dia(int i) {
		String[] diasSemana = {"lunes", "martes", "miércoles", 
				"jueves", "viernes", "sábado", "domingo"};
		System.out.printf("Hoy es %s.", diasSemana[i-1]);
		
	}
}
