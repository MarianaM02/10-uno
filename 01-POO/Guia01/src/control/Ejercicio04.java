package control;

import java.util.Scanner;

/*
 4. Se ingresa por teclado un conjunto de valores numéricos enteros 
 positivos, se pide informar, por cada uno, si el valor ingresado es 
 par o impar. Para indicar el final se ingresará un valor cero o negativo.
 */
public class Ejercicio04 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.print("Ingrese un numero: ");
		int numero = sc.nextInt();

		while (numero != 0) {
			informarSiEsPar(numero);
			System.out.print("Ingrese un numero: ");
			numero = sc.nextInt();
		}
		System.out.println("Chauchis!");
		sc.close();
	}

	private static void informarSiEsPar(int numero) {
		System.out.println(numero % 2 == 0 ? "Es par" : "Es impar");
	}

}
