package control;

/*5. Desarrollar un algoritmo que muestre por pantalla los 
 * primeros n números naturales considerando al 0 (cero) 
 * como primer número natural.
 */
public class Ejercicio05 {
	public static void main(String[] args) {
		primerosNumerosNaturales(55);
	}

	private static void primerosNumerosNaturales(int limite) {
		for (int i = 0; i < limite; i++) {
			System.out.println(i);
		}
	}
}
