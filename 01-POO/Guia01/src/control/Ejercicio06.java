package control;

import java.util.Scanner;

/*
 6. Determinar la sumatoria de los elementos de un conjunto de 
 valores numéricos. Los números se ingresaran por teclado. Se 
 ingresará un cero para finalizar.
 */

public class Ejercicio06 {
	public static void main(String[] args) {
		System.out.println("Ingrese los numeros a sumar:");
		System.out.println("Resultado: " + sumatoria());
	}

	private static int sumatoria() {
		Scanner sc = new Scanner(System.in);
		int sumatoria = 0;
		int numero = 0;
		do {
			sumatoria += numero;
			numero = sc.nextInt();
		} while (numero != 0);

		sc.close();
		return sumatoria;
	}
}
