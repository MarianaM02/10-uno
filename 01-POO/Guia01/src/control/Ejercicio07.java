package control;

import java.util.Scanner;

/*
 7. Dado un conjunto de valores numéricos que se ingresan por 
 teclado determinar el valor promedio. El fin de datos se indicará 
 ingresando un valor igual a cero.
 */
public class Ejercicio07 {
	public static void main(String[] args) {
		System.out.println("Ingrese los numeros a promediar:");
		System.out.println("Resultado: " + promedio());
	}

	private static double promedio() {
		Scanner sc = new Scanner(System.in);
		int sumatoria = 0;
		int contador = 0;
		int numero = sc.nextInt();
		while (numero != 0) {
			sumatoria += numero;
			contador++;
			numero = sc.nextInt();
		}
		sc.close();
		return (double) sumatoria / (double) contador;
	}
}
