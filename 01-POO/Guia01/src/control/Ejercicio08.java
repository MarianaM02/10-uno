package control;

public class Ejercicio08 {
	public static void main(String[] args) {

		System.out.println(esNumeroPrimo(13));
		System.out.println(esNumeroPrimo2(9));
	}

	private static boolean esNumeroPrimo(int numero) {
		if (numero < 2)
			return false;
		for (int divisor = 2; divisor < numero; divisor++) {
			if (numero % divisor == 0)
				return false;
		}
		return true;
	}

	public static boolean esNumeroPrimo2(long numero) {
		if (numero < 2)
			return false;
		for (long divisor = 2; divisor <= (long) Math.sqrt(numero); divisor++) {
			if (numero % divisor == 0)
				return false;
		}
		return true;
	}
}
