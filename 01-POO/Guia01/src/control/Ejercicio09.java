package control;

public class Ejercicio09 {
	//9. Desarrollar un algoritmo que muestre los primeros n 
	//números primos siendo n un valor que debe ingresar el usuario.
	
	public static void main(String[] args) {
		
		listaNumerosPrimos(100);
		
	}

	private static void listaNumerosPrimos(int limite) {
		int cantPrimos = 0;
		// se considera que el primer numero primo es 2
		int contador = 2;

		while (cantPrimos != limite ) {
			if (esNumeroPrimo(contador)) {
				System.out.println(contador);
				cantPrimos++;
			}
			contador++;
		}
		
	}
	
	// se consideran sólo los numeros naturales
	private static boolean esNumeroPrimo(int numero) {
		if (numero <2) return false;
		for (int divisor=2; divisor<numero; divisor++) {
			if (numero % divisor == 0)
				return false;
		}
		return true;
	}
}
