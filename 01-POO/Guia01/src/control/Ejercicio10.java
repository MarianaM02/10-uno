package control;

import java.util.Scanner;

/*
 10. Dado un conjunto de valores numéricos indicar cuál es el mayor. 
 El ingreso de datos finaliza con la llegada de un cero.
 */
public class Ejercicio10 {
	public static void main(String[] args) {
		System.out.println("Ingrese los numeros:");
		System.out.println("El mayor era " + indicaElMayor());
	}

	private static int indicaElMayor() {
		Scanner sc = new Scanner(System.in);
		int aux;
		int mayor = aux = sc.nextInt();
		while (aux != 0) {
			if (aux > mayor) {
				mayor = aux;
			}
			aux = sc.nextInt();
		}
		sc.close();
		return mayor;
	}
}
