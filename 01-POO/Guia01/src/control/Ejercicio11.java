package control;

import java.util.Scanner;

/*11. Determinar el menor valor de un conjunto de números e indicar también 
 su posición relativa dentro del mismo. El ingreso de datos finaliza con 
 la llegada de un cero.
 */
public class Ejercicio11 {
	public static void main(String[] args) {

		System.out.println("Ingrese los numeros:");
		indicaElMenor();
	}

	private static void indicaElMenor() {
		Scanner sc = new Scanner(System.in);
		int cont = 0;
		int posicion = 1;
		int aux;
		int menor = aux = sc.nextInt();
		while (aux != 0) {
			cont++;
			if (aux < menor) {
				menor = aux;
				posicion = cont;
			}
			aux = sc.nextInt();
		}
		System.out.println("El menor era " + menor);
		System.out.println("En la posición " + posicion);
		sc.close();
	}
}
