/**
 * Comentario javadoc
 * 
 * @author marian
 * @version 1.2
 * 
 * Hacer ejercicios 13, Funciones , datos alfanumericos
 * lcarnero@uno.edu.ar
 */

package control;

public class Ejercicio12 {
	public static void main(String[] args) {
		// 12. Se ingresa por consola un número entero que
		// representa un sueldo que se debe pagar. Considerando
		// que existen billetes de las denominaciones que se
		// indican más abajo; informar, que cantidad de billetes
		// de cada denominación se deberá utilizar, dando
		// prioridad a los de valor nominal más alto.
		// Denominaciones ($) = {100, 50, 20, 10, 5, 2, 1}

		darBilletes(99999);
		darBilletes(930);

	}
	/**
	 * Calcula y muestra cuántos billetes de cada denominacion
	 * se requieren para representar el valor pasado como argumento
	 * 
	 * @param sueldo
	 * @return void (sin sentido je) 
	 */
	private static void darBilletes(int sueldo) {
		int[] denominaciones = { 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1 };
		int resto = sueldo;
		System.out.printf("\nPara $%d necesito:\n", sueldo);
		for (int d : denominaciones) {
			int cantBilletes = 0;
			cantBilletes = resto / d;
			resto = resto % d;
			System.out.printf("%4d billetes de $%4d\n", cantBilletes, d);
		}
	}
}
