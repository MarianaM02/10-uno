package control;

public class EjercicioInvertirArray {
	public static void main(String[] args) {

		int[] arregloA = { 3, 6, 3, 4, 87, 21 };
		int[] arregloB = invertirArreglo(arregloA);
		
		mostrarArregloEnLinea(arregloA);
		mostrarArregloEnLinea(arregloB);
		
		String unString="Hola Mundo!";
		System.out.println(invertirString(unString));

	}

	private static void mostrarArregloEnLinea(int[] arreglo) {
		for (int i = 0; i < arreglo.length; i++) {
			System.out.print(arreglo[i] + " ");
		}
		System.out.println();
	}

	private static int[] invertirArreglo(int[] arreglo) {
		int[] arrInvertido = new int[arreglo.length];
		int subindice = 0;
		for (int i=arreglo.length-1; i >= 0; i--) {
			arrInvertido[subindice] = arreglo[i];
			subindice++;
		}
		return arrInvertido;
	}
	
	private static String invertirString(String s) {
		String nuevoString="";
		//char[] original = s.toCharArray();
		//length() -> metodo ---- length -> atributo
		for (int i = s.length() - 1; i >= 0; i--) {
			nuevoString += s.charAt(i);
		}
		return nuevoString;
	}
}
