package funciones;

import java.util.Scanner;

public class Arreglos {
	public static void main(String[] args) {
		// int[] arreglo = { 12, 43, 5, 26, 7, 98, 1, 32, 18, 9 };
		// mostrarEInvertirArreglo(arreglo);
		// invertir5Strings();
		// facturacionMensual();
		// valoresRepetidos();
		// valoresRepetidos100();
		matriculasColegio();
	}

	/**
	 * 1. Desarrollar un programa que le permita al usuario ingresar un conjunto de
	 * 10 valores enteros. Luego se debe imprimir el conjunto que el usuario
	 * ingresó, primero en el orden original y luego en el inverso. Por ejemplo, si
	 * el usuario ingresa: 12, 43, 5, 26, 7, 98, 1, 32, 18, 9 la salida del programa
	 * debe ser la siguiente: Orden original: 12 43 5 26 7 98 1 32 18 9 Orden
	 * inverso: 9 18 32 1 98 7 26 5 43 12
	 * 
	 * @param arr
	 */
	public static void mostrarEInvertirArreglo(int[] arr) {
		System.out.print("\nOrden original:");
		for (int i = 0; i < arr.length; i++)
			System.out.printf(" %d", arr[i]);
		System.out.print("\nOrden inverso:");
		for (int i = arr.length - 1; i >= 0; i--)
			System.out.printf(" %d", arr[i]);
	}

	/**
	 * 2. Desarrollar un programa que le permita al usuario ingresar un conjunto de
	 * 5 cadenas de caracteres. Luego se debe imprimir el conjunto que el usuario
	 * ingresó, pero en orden inverso.
	 */
	public static void invertir5Strings() {
		Scanner lector = new Scanner(System.in);
		String[] arr = new String[5];
		System.out.println("\n\nIntroduzca 5 Strings:");
		for (int i = 0; i < arr.length; i++)
			arr[i] = lector.nextLine();
		System.out.println("\nEn orden inverso:");
		for (int i = arr.length - 1; i >= 0; i--)
			System.out.printf("%s ", arr[i]);
		lector.close();
	}

	/**
	 * 3. Dado un conjunto de número enteros mayores o iguales a 0 y menores que 100
	 * determinar e informar cuántas veces aparece cada uno. El conjunto finaliza
	 * con la llegada de un valor negativo.
	 */
	public static void valoresRepetidos() {
		Scanner lector = new Scanner(System.in);
		int[] numeros = new int[100];
		System.out.print("Ingrese un Numero [0-99]: ");
		int num = lector.nextInt();
		while (num >= 0) {
			if (num >= 100)
				System.out.print("No esta en rango!\n");
			else
				numeros[num]++;

			System.out.print("Ingrese un Numero [0-99]: ");
			num = lector.nextInt();
		}
		System.out.print("\n************************************\n");
		for (int i = 0; i < numeros.length; i++) {
			if (numeros[i] != 0)
				System.out.printf("%2d: %3d veces\n", i, numeros[i]);
		}
		lector.close();
	}

	/**
	 * 4. Se tiene una tabla que detalla la facturación emitida por un comercio
	 * durante el período de un mes, con el siguiente formato: nroFactura (número
	 * entero de 8 dígitos) día (número entero entre 1 y 31) importe (número real)
	 * codCliente (alfanumérico, 5 caracteres) Los datos no necesariamente están
	 * ordenados y puede haber más de una factura emitida en un mismo día. Finaliza
	 * cuando se ingrese un nroFactura igual a cero. Se pide: A) Total facturado por
	 * día, sólo para aquellos días en los que hubo facturación. B) Día de mayor
	 * facturación (será único) y monto total facturado ese día.
	 */
	public static void facturacionMensual() {
		Scanner lector = new Scanner(System.in);
		int nroFactura = 0, dia = 0, mayor = 0;
		//String codCliente;
		double[] facturadoPorDia = new double[31];

		System.out.print("\n************************************\n");
		System.out.print("Ingrese Numero de Factura: ");
		nroFactura = lector.nextInt();
		while (nroFactura != 0) {
			System.out.print("Ingrese el Dia del Mes: ");
			dia = lector.nextInt();
			System.out.print("Ingrese el Importe: ");
			facturadoPorDia[dia - 1] += lector.nextDouble();
			System.out.print("Ingrese el Codigo del Cliente: ");
			//codCliente = lector.next();
			System.out.print("\n************************************\n");
			System.out.print("Ingrese Numero de Factura: ");
			nroFactura = lector.nextInt();
		}
		// A) Mostrar total facturado x dia (solo !=0)
		System.out.print("\n****************TOTAL***************\n");
		for (int i = 0; i < facturadoPorDia.length; i++) {
			if (facturadoPorDia[i] != 0)
				System.out.printf("Dia %2d: $%.2f\n", (i + 1), facturadoPorDia[i]);
		}

		// B) Mostrar Día de mayor facturación y monto total facturado ese día.
		System.out.print("\n*************MAYOR*MONTO************\n");
		for (int i = 0; i < facturadoPorDia.length; i++) {
			if (facturadoPorDia[i] > facturadoPorDia[mayor])
				mayor = i;
		}
		System.out.printf("El dia %2d fue el de mayor facturación, " + "con un total de $%.2f\n", (mayor + 1),
				facturadoPorDia[mayor]);
		lector.close();
	}

	/**
	 * 5. Dado un conjunto de número enteros determinar cuántas veces se repite cada
	 * uno. Los datos se ingresan sin ningún orden y finalizan al llegar el valor 0.
	 * Se garantiza que a los sumo habrá 100 números diferentes.
	 */
	public static void valoresRepetidos100() {
		Scanner lector = new Scanner(System.in);
		int[][] numeros = new int[20][2];
		boolean encontrado, listaLlena = false;
		System.out.print("Ingrese un Numero: ");
		int num = lector.nextInt();
		while (num != 0 && !listaLlena) {
			encontrado = false;
			for (int i = 0; i < numeros.length; i++) {
				if (num == numeros[i][0]) {
					numeros[i][1]++;
					encontrado = true;
					break;
				}
			}
			listaLlena = (numeros[numeros.length - 1][0]) != 0 ? true : false;
			if (!encontrado && !listaLlena) {
				for (int i = 0; i < numeros.length; i++) {
					if (0 == numeros[i][0]) {
						numeros[i][0] = num;
						numeros[i][1]++;
						encontrado = true;
						break;
					}
				}
			}

			System.out.print("Ingrese un Numero: ");
			num = lector.nextInt();
		}
		System.out.print("\n************************************\n");
		for (int i = 0; i < numeros.length; i++) {
			if (numeros[i][0] != 0)
				System.out.printf("%4d:\t%3d veces\n", numeros[i][0], numeros[i][1]);
		}
		lector.close();
	}

	/**
	 * 6. Se ingresa un conjunto de ternas de valores que representan el año, el
	 * grado y la cantidad de alumnos que se inscribieron en un colegio durante ese
	 * año y para ese grado en particular. Solo se ingresará la información
	 * comprendida entre los años 2000 y 2009. En el colegio, los alumnos cursan
	 * desde el primer grado hasta el séptimo. Los datos se ingresan sin ningún tipo
	 * de orden. Se pide: A) Emitir un listado ordenado por año detallando para cada
	 * año la cantidad de inscriptos por grado. B) Emitir un listado ordenado por
	 * grado detallando para cada grado la cantidad de inscriptos por año.
	 */
	public static void matriculasColegio() {
		Scanner lector = new Scanner(System.in);
		int[][] listaMatriculaciones = new int[10][7];
		int anio, grado, cantAlum;
		System.out.println("|| Matriculados | Grados 1-7 | 2000-2009 ||");
		System.out.println("Ingresar Año, Grado, Cantidad de matriculados: (Año 0 para salir)");
		anio = lector.nextInt();
		while (anio != 0) {
			grado = lector.nextInt();
			cantAlum = lector.nextInt();
			listaMatriculaciones[anio - 2000][grado - 1] = cantAlum;
			System.out.println("Ingresar Año, Grado, Cantidad de matriculados:");
			anio = lector.nextInt();
		}
		// A) Emitir un listado ordenado por año detallando para cada
		// año la cantidad de inscriptos por grado.
		System.out.print("\n************************************\n");
		for (int i = 0; i < listaMatriculaciones.length; i++) {
			System.out.println((i + 2000) + ":");
			for (int j = 0; j < listaMatriculaciones[i].length; j++) {
				System.out.println("\tGrado " + (j + 1) + "°: " + listaMatriculaciones[i][j]);
			}
		}
		// B) Emitir un listado ordenado por grado detallando para cada grado la
		// cantidad de inscriptos por año.
		// !!REVISAR
		System.out.print("\n************************************\n");
		for (int i = 0; i < listaMatriculaciones[0].length; i++) {
			System.out.println("Grado " + (i + 1) + "°: ");
			for (int j = 0; j < listaMatriculaciones.length; j++) {
				System.out.println("\t" + (j + 2000) + ":" + listaMatriculaciones[j][i]);
			}
		}
		lector.close();
	}

}
