package funciones;
public class DatosAlfanumericos {
	public static void main(String[] args) {
		
	}
	/**
	 * 1.Determinar si un char es un dígito numérico
	 * @param c
	 * @return
	 */
	public static boolean esDigito(char c) {
		return Character.isDigit(c);
	}
	/**
	 * 2. Determinar si un carácter es una letra
	 * @param c
	 * @return
	 */
	public static boolean esLetra(char c) {
		return Character.isLetter(c);
	}
	/**
	 * 3. Determinar si un carácter es una letra mayúscula 
	 * o minúscula
	 * @param c
	 * @return
	 */
	public static boolean esMayuscula(char c) {
		return Character.isUpperCase(c);
	}
	/**
	 * 3. Determinar si un carácter es una letra mayúscula 
	 * o minúscula
	 * @param c
	 * @return
	 */
	public static boolean esMinuscula(char c) {
		return Character.isLowerCase(c);
	}
	/**
	 * 4. Determinar la longitud de una cadena
	 * @param c
	 * @return
	 */
	public static int longitud(String s) {
		return s.length();
	}
	/**
	 * 5. Determinar si una cadena es vacía
	 * @param s
	 * @return
	 */
	public static boolean esVacia(String s) {
		return s.isEmpty();
		//return s.isBlank();
	}
	/**
	 * 6. Concatenar dos cadenas
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static String concatenarCadena(String s1, String s2) {
		return s1.concat(s2);
	}
	/**
	 * 7. Comparar cadenas
	 * @param s1
	 * @param s2
	 * @return
	 */
	public static int comparaCadenas(String s1, String s2) {
		return s1.compareTo(s2);
		//return s1.compareToIgnoreCase(s2);
	}
	/**
	 * 8. Dado un valor entero que representa un sueldo a pagar, 
	 * y un conjunto de denominaciones, que representan los 
	 * valores nominales de los billetes disponibles; informar 
	 * qué cantidad de billetes de cada tipo se necesitará utilizar 
	 * para abonar dicho sueldo. Dar prioridad a los billetes de 
	 * mayor denominación. 
	 * 
	 * @param sueldo
	 * @return 
	 */
	public static void darBilletes(int sueldo) {
		int[] denominaciones = { 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1 };
		int resto = sueldo;
		System.out.printf("\nPara $%d necesito:\n", sueldo);
		for (int d : denominaciones) {
			int cantBilletes = 0;
			cantBilletes = resto / d;
			resto = resto % d;
			System.out.printf("%4d billetes de $%4d\n", cantBilletes, d);
		}
	}
}
