package funciones;

/*
 1. Leer seis valores numéricos enteros. Los primeros 3 representan 
 el día, el mes y el año de una fecha, los tres restantes representan 
 los mismos atributos de otra. Se pide determinar e informar cuál de 
 las dos fechas es posterior.
 */
public class Ej01_CompararFechas {
	public static void main(String[] args) {
		fechaPosterior(2, 4, 1991, 1, 8, 1996);
	}

	private static void fechaPosterior(int dia1, int mes1, int anio1, int dia2, int mes2, int anio2) {
		int fecha1 = convertirAFechaLarga(dia1, mes1, anio1);
		int fecha2 = convertirAFechaLarga(dia2, mes2, anio2);
		
		if (fecha1 > fecha2)
			System.out.printf("El dia %d/%d/%d es posterior a %d/%d/%d.\n", dia1, mes1, anio1, dia2, mes2, anio2);
		else
			System.out.printf("El dia %d/%d/%d es posterior a %d/%d/%d.\n", dia2, mes2, anio2, dia1, mes1, anio1);

	}

	private static int convertirAFechaLarga(int dia, int mes, int anio) {
		return anio * 10000 + mes * 100 + dia;
	}
}