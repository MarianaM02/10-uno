package funciones;
/*
 3. Se dispone de un conjunto de valores que representan fechas 
 expresadas como números enteros de 8 dígitos con formato aaaammdd. 
 Se pide informar:
1. ¿Cuántas fechas corresponden al mes de marzo?
2. ¿Cuántas fechas corresponden a años bisiestos?
3. ¿Verificar que si en una fecha el día 29 y el mes es 2 entonces 
que el año sea bisiesto. En caso contrario, mostrar un mensaje de error 
e informar, al final del proceso, la cantidad de veces que se registraron 
errores de este tipo.
 */

public class Ej03_FechasBisiestos {
	public static void main(String[] args) {
		int[] fechas = { 20210831, 20200315, 19960402, 20200230 };
		int cantFechasMarzo = 0, cantFechasAniosBisiestos = 0;

		// Cuantas fechas en marzo
		for (int f : fechas) {
			if (obtenerMes(f) == 3)
				cantFechasMarzo++;
		}
		System.out.println("¿Cuántas fechas corresponden al mes de marzo?: " + cantFechasMarzo);

		// Cuantas fechas en años bisiestos
		for (int f : fechas) {
			if (esBisiesto(f))
				cantFechasAniosBisiestos++;
		}
		System.out.println("¿Cuántas fechas corresponden a años bisiestos?: " + cantFechasAniosBisiestos);

		// Fechas correctas en años bisiestos
		for (int f : fechas) {
			if (obtenerMes(f) == 2 && esBisiesto(f) && obtenerDia(f) > 28) {
				System.out.printf("La fecha %d/%d/%d no es válida\n", obtenerDia(f), obtenerMes(f), obtenerAnio(f));
			}
		}
	}

	private static int obtenerDia(int unaFecha) {
		return unaFecha % 100;
	}

	private static int obtenerMes(int unaFecha) {
		return unaFecha / 100 % 100;
	}

	private static int obtenerAnio(int unaFecha) {
		return unaFecha / 10000;
	}

	private static boolean esBisiesto(int unaFecha) {
		return obtenerAnio(unaFecha) % 4 == 0;
	}

}
