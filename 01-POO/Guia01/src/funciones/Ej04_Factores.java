package funciones;

/*
 4. Desarrollar una función que reciba un valor numérico y que en cada 
 invocación sucesiva retorne cada uno de sus factores. El valor de 
 retorno de la función debe ser booleano para indicar si se pudo 
 obtener un nuevo factor o no.
 */
public class Ej04_Factores {
	public static void main(String[] args) {
		encontrarFactores(48);
		encontrarFactores(52);
		encontrarFactores(89);
		encontrarFactores(6);
		encontrarFactores(3);
		encontrarFactores(9);
		encontrarFactores(1989797656);
		
	}

	private static boolean encontrarFactores(int numero) {
		int n;
		if (esNumeroPrimo(numero))
			n = numero;
		else
			n = (int)Math.sqrt(numero);
		for (int divisor = n; divisor > 1; divisor--) {
			if (esNumeroPrimo(divisor) && esDivisor(numero, divisor)) {
				System.out.printf("%12d|%4d\n", numero, divisor);
				encontrarFactores(numero / divisor);
				return true;
			}
		}
		System.out.printf("%12d|\n\n", 1);
		return false;
	}

	private static boolean esNumeroPrimo(int numero) {
		if (numero < 2)
			return false;
		for (int divisor = 2; divisor <= Math.sqrt(numero); divisor++) {
			if (numero % divisor == 0)
				return false;
		}
		return true;
	}

	private static boolean esDivisor(int numero, int divisor) {
		return numero % divisor == 0;
	}
}
