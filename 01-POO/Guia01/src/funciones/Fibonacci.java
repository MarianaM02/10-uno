package funciones;

/*
 0 1 1 2 3 5 8 13 21
 */
public class Fibonacci {
	public static void main(String[] args) {
		System.out.println(fibonacci(4));
	}

	private static int fibonacci(int num) {
		if (num < 2)
			return num;
		else
			return fibonacci(num - 1) + fibonacci(num - 2);
	}
}
