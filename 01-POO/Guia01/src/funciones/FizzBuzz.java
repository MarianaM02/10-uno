package funciones;

import java.util.stream.IntStream;

@SuppressWarnings("unused")
public class FizzBuzz {
	
	public static void main(String[] args) {
		fizzBuzz(100);
		serieAcumulacion(100);
		serieGauss(100);
	}
	
	private static void fizzBuzz(int limite) {
		for (int i=1; i<limite; i++) {
			String valor = Integer.toString(i);
			if(i%3 == 0) valor = "Fizz";
			if(i%5 == 0) valor = "Buzz";
			if(i%15 == 0) valor = "FizzBuzz";
			System.out.println(valor);
		}
	}
	
	
	private static void fizzBuzz2(int limite) {
		for (int i=1; i<=limite; i++) {
			String valor="";
			if(i%3 == 0) valor = "Fizz";
			if(i%5 == 0) valor += "Buzz";
			
			System.out.println(valor.equals("") ? i : valor);
		}
	}
	
	private static void fizzBuzz3Compa() {
		 IntStream.rangeClosed(1, 100).mapToObj(i->i%3==0?(i%5==0? "FizzBuzz ":"Fizz "):(i%5==0? "Buzz ": i+" ")).forEach(System.out::println);  
	}
	
	private static void fizzBuzz4(int limite) {
		for (int i=1; i<limite; i++) {
			String valor;
			if(i%15 == 0) {
				System.out.println("FizzBuzz");
				continue;
			}
			if(i%5 == 0) {
				System.out.println("Buzz");
				continue;
			}
			if(i%3 == 0) {
				System.out.println("Fizz");
				continue;
			}
			
			System.out.println(i);
		}
	}
	
	private static void serieAcumulacion(int limite) {
		int sumatoria=0;
		for (int i=1; i<=limite; i++) {
			sumatoria += i;
			System.out.printf("%4d | %7d\n",i,sumatoria);
		}
	}
	
	public static int serieGauss(int n) {
		return (n*(n+1))/2;
	}
	
}
