package introduccion;

import java.util.Scanner;

public class Ejercicio01 {
	public static void main(String[] args) {
		// 1. Leer dos valores enteros e informar la suma.

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Ingrese el primer n�mero entero:");
		int num1 = keyboard.nextInt();
		System.out.println("Ingrese un segundo n�mero entero:");
		int num2 = keyboard.nextInt();
		keyboard.close();
		int suma = num1 + num2;
		System.out.printf("La suma de %d m�s %d es igual a %d", num1, num2, suma);

	}
}
