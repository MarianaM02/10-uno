package introduccion;

import java.util.Scanner;

public class Ejercicio02 {
	public static void main(String[] args) {
		// 2. Leer dos valores num�ricos enteros e informar su cociente.

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Ingrese el primer n�mero entero:");
		int num1 = keyboard.nextInt();
		System.out.println("Ingrese un segundo n�mero entero:");
		int num2 = keyboard.nextInt();
		keyboard.close();
		int cociente = num1 / num2; // Cociente entero
		System.out.printf("El cociente de %d entre %d es igual a %d", num1, num2, cociente);
	}
}
