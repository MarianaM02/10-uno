package introduccion;

import java.util.Scanner;

public class Ejercicio03 {

	public static void main(String[] args) {
		// 3. Dado un valor num�rico entero, informar si es par o impar.

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Ingresar un numero: ");
		int num = keyboard.nextInt();
		keyboard.close();
		System.out.println(num % 2 == 0 ? "Es par" : "Es impar");

	}

}
