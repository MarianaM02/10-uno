package introduccion;

import java.util.Scanner;

public class Ejercicio04 {
	public static void main(String[] args) {
		/*
		 * 4. Se ingresa un valor num�rico de 8 d�gitos que representa una fecha con el
		 * siguiente formato aaaammdd. Se pide informar por separado el d�a, el mes y el
		 * a�o de la fecha ingresada.
		 */

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Ingresar fecha en formato aaaammdd ");
		int fechaCompleta = keyboard.nextInt();
		keyboard.close();
		int anio = fechaCompleta / 10000;
		int mes = fechaCompleta / 100 % 100;
		int dia = fechaCompleta % 100;

		System.out.printf("Dia: %d, Mes: %d, A�o: %d", dia, mes, anio);

	}
}
