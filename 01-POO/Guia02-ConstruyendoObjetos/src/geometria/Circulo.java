package geometria;

public class Circulo {
	private double radio;

	public Circulo(double radio) {
		if(radio<=0) {
			throw new Error ("Radio Inválido!");
		}
		this.radio = radio;
	}

	public double getRadio() {
		return radio;
	}

	public void setRadio(double nuevoRadio) {
		radio = nuevoRadio;
	}

	public double getDiametro() {
		return radio * 2;
	}
	
	public void setDiametro(double diametro) {
		this.radio = diametro / 2;
	}

	public double getArea() {
		return Math.PI * Math.pow(radio, 2);
	}

	public double getPerimetro() {
		return 2 * Math.PI * radio;
	}

}
