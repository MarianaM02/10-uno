package geometria;

public class Cubo {
	private double lado;

	/**
	 * post: inicializa el cubo a partir de la medida de lado dada
	 */
	public Cubo(double lado) {
		this.lado = lado;
	}

	/**
	 * post: devuelve la longitud de todos los lados del cubo
	 */
	public double medirLongitudLado() {
		return lado;
	}

	/**
	 * pre: lado es un valor mayor a 0.
	 * 
	 * post: cambia la longitud de todos los lados del cubo
	 */
	public void cambiarLongitudLado(double lado) {
		if (lado > 0)
			this.lado = lado;
		else
			System.out.println("Valor Inválido!");
	}

	/**
	 * post: devuelve la superficie ocupada por las caras del cubo
	 */
	public double medirSuperficieCara() {
		return Math.pow(lado, 2) * 6;
	}

	/**
	 * pre: superficieCara es un valor mayor a 0.
	 * 
	 * post: cambia la superficie de las caras del cubo
	 */
	public void cambiarSuperficieCara(double superficieCara) {
		if (superficieCara > 0)
			this.lado = Math.sqrt(superficieCara / 6);
		else
			System.out.println("Valor Inválido!");
	}

	/**
	 * post: devuelve el volumen que encierra el cubo
	 */
	public double medirVolumen() {
		return Math.pow(lado, 3);
	}

	/*
	 * pre: volumen es un valor mayor a 0.
	 * 
	 * post: cambia el volumen del cubo
	 */
	public void cambiarVolumen(double volumen) {
		if (volumen > 0)
			this.lado = Math.cbrt(volumen);
		else
			System.out.println("Valor Inv�lido!");
	}
}
