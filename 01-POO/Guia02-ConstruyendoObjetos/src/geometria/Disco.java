package geometria;

public class Disco {
	private double radioInterior;
	private double radioExterior;

	public Disco(double radioInterior, double radioExterior) {
		this.radioInterior = radioInterior;
		this.radioExterior = radioExterior;
	}

	public double getRadioInterior() {
		return radioInterior;
	}

	public void setRadioInterior(double radioInterior) {
		this.radioInterior = radioInterior;
	}

	public double getRadioExterior() {
		return radioExterior;
	}

	public void setRadioExterior(double radioExterior) {
		this.radioExterior = radioExterior;
	}

	public double calcularArea() {
		return (Math.PI * (Math.pow((radioExterior), 2) - Math.pow((radioInterior), 2)));
	}

	public double getPerimetroInterior() {
		return 2 * Math.PI * radioInterior;
	}

	public double getPerimetroExterior() {
		return 2 * Math.PI * radioExterior;
	}

}
