package geometria;

public class Disco2 {
	private Circulo circInterior;
	private Circulo circExterior;

	public Disco2(double radioInterior, double radioExterior) {
		if (radioExterior > radioInterior) {
			this.circInterior = new Circulo(radioInterior);
			this.circExterior = new Circulo(radioExterior);
		} else {
			this.circInterior = new Circulo(radioExterior);
			this.circExterior = new Circulo(radioInterior);
		}
	}

	public double getRadioInterior() {
		return circInterior.getRadio();
	}

	public void setRadioInterior(double radioInterior) {
		circInterior.setRadio(radioInterior);
	}

	public double getRadioExterior() {
		return circExterior.getRadio();
	}

	public void setRadioExterior(double radioExterior) {
		circExterior.setRadio(radioExterior);
		;
	}

	public double getArea() {
		return circExterior.getArea() - circInterior.getArea();
	}

	public double getPerimetroInterior() {
		return circInterior.getPerimetro();
	}

	public double getPerimetroExterior() {
		return circExterior.getPerimetro();
	}

}
