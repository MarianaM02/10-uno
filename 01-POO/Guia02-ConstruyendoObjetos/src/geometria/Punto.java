package geometria;

import static java.lang.Math.*;

public class Punto {
	private Double x;
	private Double y;

	public Punto(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double obtenerX() {
		return x;
	}

	public void cambiarX(double x) {
		this.x = x;
	}

	public double obtenerY() {
		return y;
	}

	public void cambiarY(double y) {
		this.y = y;
	}

	public boolean estaSobreEjeY() {
		return x == 0;
	}

	public boolean estaSobreEjeX() {
		return y == 0;
	}

	public boolean esCoordenadaOrigen() {
		return (estaSobreEjeX() && estaSobreEjeY());
	}

	public double distanciaA(Punto p) {
		double diferenciaEnX = this.obtenerX() - p.obtenerX();
		double diferenciaEnY = this.obtenerY() - p.obtenerY();
		return sqrt(pow(diferenciaEnX, 2) + pow(diferenciaEnY, 2));
	}

}
