package objetos;

public class Alarma {
	private boolean encendido;
	private int codigoSeguridad;
	private Sensor sensor;

	/**
	 * post: alarma apagada con el código de seguridad indicado.
	 */
	public Alarma(int codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
		this.encendido = false;
	}
	
	/**
	 * post: devuelve encendido
	 *  
	 * @return
	 */
	public boolean estaEncendido() {
		return encendido;
	}

	/**
	 * post: enciende la alarma.
	 */
	public void encender() {
		this.encendido = true;
	}

	/**
	 * post: si codigoSeguridad es correcto, apaga la alarma.
	 */
	public void apagar(int codigoSeguridad) {
		if (this.codigoSeguridad == codigoSeguridad && this.encendido)
			this.encendido = false;
	}

	/**
	 * post: devuelve si alguno de los sensores está activado.
	 */
	public boolean activada() {
		return this.sensor.activado();
	}

}
