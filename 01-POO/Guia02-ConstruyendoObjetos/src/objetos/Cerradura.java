package objetos;

public class Cerradura {
	private int clave;
	private int maxFallos;
	private int cantIntentosFallidos;
	private boolean estaAbierta;
	private int cantAperturas;

	/**
	 * post: seteo de la clave y la cantidad de fallos maximos posibles.
	 * Inicializados los contadores y bandera.
	 * 
	 * @param claveDeApertura
	 * @param cantidadDeFallosConsecutivosQueLaBloquean
	 */
	public Cerradura(int claveDeApertura, int cantidadDeFallosConsecutivosQueLaBloquean) {
		this.clave = claveDeApertura;
		this.maxFallos = cantidadDeFallosConsecutivosQueLaBloquean;
		this.cantIntentosFallidos = 0;
		this.cantAperturas = 0;
		this.estaAbierta = false;
	}

	/**
	 * pre: candado no bloqueado o abierto. Clave igual a clave seteada.
	 * 
	 * post: abre el candado segun clave.
	 * 
	 * @param clave
	 * @return
	 */
	public boolean abrir(int clave) {
		if (fueBloqueada()) {
			System.out.println("La cerradura esta bloqueada.");
			return false;
		}
		if (estaAbierta) {
			System.out.println("La cerradura ya esta abierta.");
			return false;
		}
		if (this.clave == clave) {
			this.estaAbierta = true;
			this.cantAperturas++;
			this.cantIntentosFallidos = 0;
			System.out.println("Abrió. Clave correcta.");
			return true;
		} else {
			System.out.println("Clave incorrecta.");
			this.cantIntentosFallidos++;
			return false;
		}
	}

	/**
	 * pre: candado abierto.
	 * 
	 * post: cierra el candado, resetea contador de fallos.
	 */
	public void cerrar() {
		if (estaAbierta) {
			this.cantIntentosFallidos = 0;
			this.estaAbierta = false;
			System.out.println("La cerradura se ha cerrado.");
		}
	}

	/**
	 * post: devuelve si el candado esta abierto
	 * 
	 * @return
	 */
	public boolean estaAbierta() {
		return estaAbierta;
	}

	/**
	 * post: devuelve si el candado esta cerrado
	 * 
	 * @return
	 */
	public boolean estaCerrada() {
		return !estaAbierta;
	}

	/**
	 * post: devuelve si el candado fue bloqueado
	 * 
	 * @return
	 */
	public boolean fueBloqueada() {
		return cantIntentosFallidos == maxFallos;
	}

	/**
	 * post: devuelve cantidad de veces que se abrió el candado
	 * 
	 * @return
	 */
	public int contarAperturasExitosas() {
		return cantAperturas;
	}

	/**
	 * post: devuelve cantidad de veces que se falló al abrir el candado
	 * 
	 * @return
	 */
	public int contarAperturasFallidas() {
		return cantIntentosFallidos;
	}
}
