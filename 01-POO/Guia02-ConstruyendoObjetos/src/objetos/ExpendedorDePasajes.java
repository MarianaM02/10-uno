package objetos;

public class ExpendedorDePasajes {
	private double precioPorKm;
	private int cantVendida;
	private double totalKm;
	private double mayorDistancia;

	/**
	 * post: setea el precio por km e inicializa contadores en 0
	 * 
	 * @param precioPorKm
	 */
	public ExpendedorDePasajes(double precioPorKm) {
		this.precioPorKm = precioPorKm;
		this.cantVendida = 0;
		this.totalKm = 0;
		this.mayorDistancia = 0;
	}

	/**
	 * pre: distancia mayor a 0
	 * 
	 * post: actualiza contadores, devuelve el precio de un pasaje
	 * 
	 * @param distanciaEnKm
	 * @return
	 */
	public double venderPasaje(double distanciaEnKm) {
		if (distanciaEnKm > 0) {
			actualizarContadores(1, distanciaEnKm);
			double precio = distanciaEnKm * this.precioPorKm;
			return precio;
		}
		return 0;
	}

	/**
	 * pre: distancia y cantidad mayor a 0
	 * 
	 * post: actualiza contadores, devuelve el precio de la cantidad de pasajes
	 * declarados
	 * 
	 * @param cantidad
	 * @param distanciaEnKm
	 * @return
	 */
	public double venderPasajes(int cantidad, double distanciaEnKm) {
		if (distanciaEnKm > 0 && cantidad > 0) {
			actualizarContadores(cantidad, distanciaEnKm);
			double precio = distanciaEnKm * this.precioPorKm * cantidad;
			return precio;
		}
		return 0;
	}

	/**
	 * post: actualiza cantidad de pasjes vendida, el total de km y si la distanci
	 * es la mayor recorrida
	 * 
	 * @param cantidad
	 * @param distanciaEnKm
	 */
	private void actualizarContadores(int cantidad, double distanciaEnKm) {
		this.cantVendida += cantidad;
		this.totalKm += distanciaEnKm * cantidad;
		if (distanciaEnKm > this.mayorDistancia)
			this.mayorDistancia = distanciaEnKm;
	}

	/**
	 * post: devuelve la cantidad de pasajes vendida
	 * 
	 * @return
	 */
	public int pasajesVendidos() {
		return cantVendida;
	}

	/**
	 * post: devuelve la mayor distancia recorrida
	 * 
	 * @return
	 */
	public double distanciaMaxima() {
		return this.mayorDistancia;
	}

	/**
	 * post: devuelve el promedio de distancia de los pasajes
	 * @return
	 */
	public double distanciaPromedio() {
		return this.totalKm / this.cantVendida;
	}

	/**
	 * post: devuelve la recaudacion total
	 * @return
	 */
	public double ventaTotal() {
		return this.totalKm * this.precioPorKm;
	}

}
