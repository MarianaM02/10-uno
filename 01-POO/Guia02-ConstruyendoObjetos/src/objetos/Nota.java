package objetos;

public class Nota {
	private int valor;

	public Nota(int valor) {
		this.valor = valor;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public boolean aprobado() {
		return (valor >= 4 && valor <= 10);
	}

	public boolean desaprobado() {
		return !aprobado();
	}

	public void recuperar(int nuevoValor) {
		if (nuevoValor > valor)
			valor = nuevoValor;
	}
	public boolean promociona() {
		return (valor >= 7);
	}

}
