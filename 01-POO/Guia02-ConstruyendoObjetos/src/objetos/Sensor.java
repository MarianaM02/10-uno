package objetos;

public class Sensor {
	private boolean estaEncendido;
	private boolean estaActivado;
	/**
	 * post: sensor apagado.
	 */
	public Sensor() {
		this.apagar();
	}

	/**
	 * post: enciende el sensor.
	 */
	public void encender() {
		this.estaEncendido = true;
	}

	/**
	 * post: apaga el sensor.
	 */
	public void apagar() {
		this.estaEncendido = false;
		this.estaActivado = false;
	}

	/**
	 * post: devuelve si el sensor ha sido activado a causa de algun evento.
	 */
	public boolean activado() {
		return this.estaActivado;
	}

	/**
	 * pre: el sensor esta prendido
	 * 
	 * post: activa el sensor.
	 */
	public void activar() {
		if (this.estaEncendido)
			this.estaActivado = true;
	}

}
