package objetos;

public class Tambor {
	private int posicion = 1;
	
	/**
	 * post: devuelve el número de posición en la que se encuentra el Tambor. Es un
	 * valor comprendido entre 1 y 8.
	 */
	public int obtenerPosicion() {
		return this.posicion;
	}

	/**
	 * post: hace girar el tambor y luego se detiene en una posición comprendida
	 * entre 1 y 8.
	 */
	public void girar() {
		this.posicion = (int) (Math.random() * 8 + 1);
	}

}
