package objetos;

public class TarjetaBaja {
	private double boletoColectivo = 1.25;
	private double boletoSubte = 2.5;
	private double saldo;
	private int cantViajesSubte;
	private int cantViajesColectivo;

	/**
	 * post: saldo de la Tarjeta en saldoInicial.
	 */
	public TarjetaBaja(double saldoInicial) {
		this.saldo = saldoInicial;
		this.cantViajesSubte = 0;
		this.cantViajesColectivo = 0;
	}

	public double obtenerSaldo() {
		return this.saldo;
	}

	/**
	 * post: agrega el monto al saldo de la Tarjeta.
	 */
	public void cargar(double monto) {
		this.saldo += monto;
	}

	/**
	 * pre : saldo suficiente.
	 * 
	 * post: utiliza 1.25 del saldo para un viaje en colectivo.
	 */
	public void pagarViajeEnColectivo() {
		if (saldo >= boletoColectivo) {
			this.saldo -= boletoColectivo;
			this.cantViajesColectivo++;
		} else {
			System.out.println("Saldo Insuficiente!");
		}
	}

	/**
	 * pre : saldo suficiente.
	 * 
	 * post: utiliza 2.50 del saldo para un viaje en subte.
	 */
	public void pagarViajeEnSubte() {
		if (saldo >= boletoSubte) {
			this.saldo -= boletoSubte;
			this.cantViajesSubte++;
		} else {
			System.out.println("Saldo Insuficiente!");
		}
	}

	/**
	 * post: devuelve la cantidad de viajes realizados.
	 */
	public int contarViajes() {
		return this.cantViajesSubte + this.cantViajesColectivo;
	}

	/**
	 * post: devuelve la cantidad de viajes en colectivo.
	 */
	public int contarViajesEnColectivo() {
		return this.cantViajesColectivo;
	}

	/**
	 * post: devuelve la cantidad de viajes en subte.
	 */
	public int contarViajesEnSubte() {
		return this.cantViajesSubte;
	}

	/**
	 * post: devuelve el precio de un boleto de colectivo
	 * 
	 * @return
	 */
	public double obtenerBoletoColectivo() {
		return boletoColectivo;
	}

	/**
	 * post: cambia el precio de un boleto de colectivo
	 * 
	 * @param boletoColectivo
	 */
	public void cambiarBoletoColectivo(double boletoColectivo) {
		this.boletoColectivo = boletoColectivo;
	}

	/**
	 * post: devuelve el precio de un boleto de subte
	 * 
	 * @return
	 */
	public double obtenerBoletoSubte() {
		return boletoSubte;
	}

	/**
	 * post: cambia el precio de un boleto de subte
	 * 
	 * @param boletoSubte
	 */
	public void cambiarBoletoSubte(double boletoSubte) {
		this.boletoSubte = boletoSubte;
	}

}