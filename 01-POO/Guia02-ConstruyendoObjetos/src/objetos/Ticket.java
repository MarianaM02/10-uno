package objetos;

public class Ticket {

	private double importe;
	private int cantProductos;
	private boolean estaAbierto;
	private boolean conDescuento;

	/**
	 * post: el Ticket se inicializa con importe 0.
	 */
	public Ticket() {
		this.importe = 0;
		this.cantProductos = 0;
		this.estaAbierto = true;
		this.conDescuento = false;
	}

	/**
	 * pre : cantidad y precio son mayores a cero. El ticket está abierto.
	 * 
	 * post: suma al Ticket un item a partir de la cantidad de productos y su precio
	 * unitario.
	 */
	public void agregarItem(int cantidad, double precioUnitario) {
		if (precioUnitario > 0 && cantidad > 0 && estaAbierto) {
			this.importe += cantidad * precioUnitario;
			this.cantProductos += cantidad;
		}
	}

	/**
	 * pre : el Ticket está abierto y no se ha aplicado un descuento previamente.
	 * 
	 * post: aplica un descuento sobre el total del importe.
	 */
	public void aplicarDescuento(double porcentaje) {
		if (estaAbierto && !conDescuento) {
			this.importe *= (100-porcentaje) / 100;
			this.conDescuento = true;
		}
	}

	/**
	 * post: devuelve el importe acumulado hasta el momento sin cerrar el Ticket.
	 */
	public double calcularSubtotal() {
		return this.importe;
	}

	/**
	 * post: cierra el Ticket y devuelve el importe total.
	 */
	public double calcularTotal() {
		this.estaAbierto = false;
		return this.importe;
	}

	/**
	 * post: devuelve la cantidad total de productos.
	 */
	public int contarProductos() {
		return this.cantProductos;
	}
}