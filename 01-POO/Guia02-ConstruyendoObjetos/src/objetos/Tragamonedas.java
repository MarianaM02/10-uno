package objetos;

public class Tragamonedas {
	private Tambor[] tambores = new Tambor[3];

	/**
	 * post: los 3 Tambores del Tragamonedas estan en la posición 1.
	 */
	public Tragamonedas() {
		for (int i = 0; i < tambores.length; i++) {
			tambores[i] = new Tambor();
		}
	}

	/**
	 * post: activa el Tragamonedas haciendo girar sus 3 Tambores.
	 */
	public void activar() {
		for (Tambor t : tambores) {
			t.girar();
		}
	}

	/**
	 * post: indica si el Tragamonedas entrega un premio a partir de la posición de
	 * sus 3 Tambores.
	 */
	public boolean entregaPremio() {
		int posInicial = tambores[0].obtenerPosicion();
		for (Tambor t : tambores) {
			if (posInicial != t.obtenerPosicion()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * post: imprime los valores de los tambores del tragamonedas e indica si se
	 * gano
	 */

	public String toString() {
		String s = "|";
		for (Tambor t : tambores) {
			s += " " + t.obtenerPosicion() + " |";
		}
		s += this.entregaPremio() ? "	GANASTE!!!! :D" : "	Mala Suerte :(";
		return s;
	}
	public static void main(String[] args) {
		Tragamonedas t1 = new Tragamonedas();
		
		System.out.println(t1);
		t1.activar();
		System.out.println(t1);
	}

}
