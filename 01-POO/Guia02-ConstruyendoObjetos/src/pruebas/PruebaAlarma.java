package pruebas;

import objetos.Alarma;

public class PruebaAlarma {
	public static void main(String[] args) {
		Alarma a1 = new Alarma(76648);
		System.out.println("Encendido: " + a1.estaEncendido());
		a1.encender();
		a1.apagar(0);
		System.out.println("Encendido: " + a1.estaEncendido());
		a1.apagar(76648);
		System.out.println("Encendido: " + a1.estaEncendido());
		
		
		
	}
}
