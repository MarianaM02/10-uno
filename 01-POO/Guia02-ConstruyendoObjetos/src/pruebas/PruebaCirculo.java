package pruebas;

import geometria.Circulo;

public class PruebaCirculo {
	public static void main(String[] args) {
		Circulo cir1 = new Circulo(4.5);
		// funciona si el main esta en la misma class
		// cir1.radio = 9;
		System.out.println("--- Prueba circulo ---");
		System.out.println("Radio = " + cir1.getRadio());
		System.out.println("Area = " + cir1.getArea());
		System.out.println("Perimetro = " + cir1.getPerimetro());
		System.out.println("Diametro = " + cir1.getDiametro());

	}
}
