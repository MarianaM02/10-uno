package pruebas;

import geometria.Cubo;

public class PruebaCubo {
	public static void main(String[] args) {
		System.out.println("\n\n--- Prueba cubo ---");
		Cubo cub1 = new Cubo(5);
		System.out.println("Lado = " + cub1.medirLongitudLado());
		System.out.println("Area = " + cub1.medirSuperficieCara());
		System.out.println("Volumen = " + cub1.medirVolumen());
		System.out.println("\n- Cambio Superficie -");
		cub1.cambiarSuperficieCara(10);
		System.out.println("Lado = " + cub1.medirLongitudLado());
		System.out.println("Area = " + cub1.medirSuperficieCara());
		System.out.println("Volumen = " + cub1.medirVolumen());
		System.out.println("\n- Cambio Volumen -");
		cub1.cambiarVolumen(15);
		System.out.println("Lado = " + cub1.medirLongitudLado());
		System.out.println("Area = " + cub1.medirSuperficieCara());
		System.out.println("Volumen = " + cub1.medirVolumen());
	}
}
