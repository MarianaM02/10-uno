package pruebas;

import geometria.Disco2;

public class PruebaDisco {
	public static void main(String[] args) {
		Disco2 d1 = new Disco2(3, 5);

		System.out.println("----Disco----");
		System.out.println("Radio Interno: " + d1.getRadioInterior());
		System.out.println("Radio Externo: " + d1.getRadioExterior());
		System.out.println("Area: " + d1.getArea());
		System.out.println("Perimetro Interno: " + d1.getPerimetroInterior());
		System.out.println("Perimetro Externo: " + d1.getPerimetroExterior());
		System.out.println();

		System.out.println("Cambiar Radio Interno: ");
		d1.setRadioInterior(2);
		System.out.println("Radio Interno: " + d1.getRadioInterior());
		System.out.println("Radio Externo: " + d1.getRadioExterior());
		System.out.println("Area: " + d1.getArea());
		System.out.println("Perimetro Interno: " + d1.getPerimetroInterior());
		System.out.println("Perimetro Externo: " + d1.getPerimetroExterior());
		System.out.println();

		System.out.println("Cambiar Radio Externo: ");
		d1.setRadioExterior(7);
		System.out.println("Radio Interno: " + d1.getRadioInterior());
		System.out.println("Radio Externo: " + d1.getRadioExterior());
		System.out.println("Area: " + d1.getArea());
		System.out.println("Perimetro Interno: " + d1.getPerimetroInterior());
		System.out.println("Perimetro Externo: " + d1.getPerimetroExterior());
	}
}
