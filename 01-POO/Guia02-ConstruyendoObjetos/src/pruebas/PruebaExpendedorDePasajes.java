package pruebas;

import objetos.ExpendedorDePasajes;

public class PruebaExpendedorDePasajes {
	public static void main(String[] args) {
		ExpendedorDePasajes e1 = new ExpendedorDePasajes(100);

		System.out.println("Pasajes Vendidos: " + e1.pasajesVendidos());
		e1.venderPasaje(300);
		e1.venderPasajes(6, 200);
		System.out.println("Pasajes Vendidos: " + e1.pasajesVendidos());
		System.out.println("Ganancia: $" + e1.ventaTotal());
		System.out.println("Viaje mas largo: " + e1.distanciaMaxima());
		System.out.println("Distancia Promedio: " + e1.distanciaPromedio());

		e1.venderPasajes(4, 400);
		System.out.println("Pasajes Vendidos: " + e1.pasajesVendidos());
		System.out.println("Ganancia: $" + e1.ventaTotal());
		System.out.println("Viaje mas largo: " + e1.distanciaMaxima());
		System.out.println("Distancia Promedio: " + e1.distanciaPromedio());

	}
}
