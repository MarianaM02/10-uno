package pruebas;

import objetos.Nota;

public class PruebaNota {
	public static void main(String[] args) {

		Nota parcial1 = new Nota(3);
		Nota parcial2 = new Nota(4);
		Nota exFinal = new Nota(9);
		System.out.println(parcial1.aprobado());
		System.out.println(parcial2.aprobado());
		parcial1.recuperar(6);
		System.out.println(parcial1.aprobado());
		System.out.println(exFinal.aprobado());

	}
}
