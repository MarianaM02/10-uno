package pruebas;

import geometria.Punto;

public class PruebaPunto {
	public static void main(String[] args) {
		Punto p1 = new Punto(20, 50);
		Punto p2 = new Punto(12, -40);

		System.out.println("----Punto 1----");
		System.out.println("x: " + p1.obtenerX());
		System.out.println("y: " + p1.obtenerY());
		System.out.println("----Punto 2----");
		System.out.println("x: " + p2.obtenerX());
		System.out.println("y: " + p2.obtenerY());
		System.out.println();
		System.out.println("----Punto 1----");
		System.out.println("Cambiar x: ");
		p1.cambiarX(0);
		System.out.println("x: " + p1.obtenerX());
		System.out.println("y: " + p1.obtenerY());
		System.out.println("Cambiar Y: ");
		p1.cambiarY(9);
		System.out.println("x: " + p1.obtenerX());
		System.out.println("y: " + p1.obtenerY());

		System.out.println("Esta punto uno sobre X?:" + p1.estaSobreEjeX());
		System.out.println("Esta punto uno sobre Y?:" + p1.estaSobreEjeY());
		System.out.println("Es coodenada origen?:" + p1.esCoordenadaOrigen());

	}
}
