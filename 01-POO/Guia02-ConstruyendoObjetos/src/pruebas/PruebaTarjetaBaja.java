package pruebas;

import objetos.TarjetaBaja;

public class PruebaTarjetaBaja {
	public static void main(String[] args) {
		TarjetaBaja t1 = new TarjetaBaja(500);

		System.out.println("Saldo: $" + t1.obtenerSaldo());
		t1.cargar(300);
		System.out.println("Saldo: $" + t1.obtenerSaldo());
		t1.pagarViajeEnSubte();
		System.out.println("Saldo: $" + t1.obtenerSaldo());
		t1.pagarViajeEnColectivo();
		System.out.println("Saldo: $" + t1.obtenerSaldo());
		System.out.println("Viajes: "+ t1.contarViajes());
		System.out.println("Viajes cole: "+ t1.contarViajesEnColectivo());
		System.out.println("Viajes subte: "+ t1.contarViajesEnSubte());
	}
}
