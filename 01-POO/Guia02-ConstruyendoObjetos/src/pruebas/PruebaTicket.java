package pruebas;

import objetos.Ticket;

public class PruebaTicket {
	public static void main(String[] args) {
		Ticket t1 = new Ticket();
		
		System.out.println("----Ticket----");
		System.out.println("Cant Items: "+ t1.contarProductos());
		System.out.println("Subtotal: $"+ t1.calcularSubtotal());
		t1.agregarItem(1, 6);
		t1.agregarItem(7, 50);
		t1.agregarItem(4, 20);
		t1.agregarItem(2, 20);
		System.out.println("Agregados productos.");
		System.out.println("Cant Items: "+ t1.contarProductos());
		System.out.println("Subtotal: $"+ t1.calcularSubtotal());
		t1.aplicarDescuento(20);
		System.out.println("Aplicado Descuento.");
		System.out.println("Subtotal: $"+ t1.calcularSubtotal());
		System.out.println("Total: $"+ t1.calcularTotal());
		t1.agregarItem(7, 50);
		t1.agregarItem(4, 20);
		t1.agregarItem(2, 20);
		System.out.println("Aplicado Descuento.");
		System.out.println("Subtotal: $"+ t1.calcularSubtotal());
	}
}
