/**
 * Programa "Hola Mundo" (y algo más)
 * 
 * - Muestra por pantalla el mensaje "Hola Mundo"
 * - Contiene ejemplos de declaración de distintos tipos de variables,
 *    uso de estructuras de decisión y de repetición. 
 *   Durante la clase buscamod similitudes y diferencias con el lenguaje C,
 *    trabajado en la materia Algoritmos y estructuras de datos.
 */

// Indica a qué paquete pertenece el archivo/clase
package holamundo;			

// Define la clase "HolaMundo"
public class HolaMundo {	
  
  // Función principal: se ejecuta al iniciarse el programa
	public static void main(String[] args) { 
		// Definición de variables
		// [tipo de dato] [identificador]
		int edad;
		edad = 30;
		
		double precio = 4.25;
		
    // Define una cadena de caracteres 
		String nombre = "Lucas"; 

    // Define una variable booleana
		boolean esMayorDeEdad = true; 
		
    // Estructura de decisión if
		if (edad >= 0) {
			
		} else {
			
		}
		
		// Estructura de repetición for
		for(int k = 1; k <= 10; k++) {
			System.out.println("k"); // Muestra 10 veces la letra k
			System.out.println(k);   // Muestra el valor de la variable k
		}
		
    // Estructura de repetición while
    // P.D.: ¿qué pasa si se comenta la línea j--?
		int j = 10;
		while (j >= 0) {
			System.out.println("j");
			j--;
		}
		
    // Estructura de repetición do...while
    // P.D.: ¿cuál era la diferencia con la estructura while?
		int i = 0;
		do {
			System.out.println(i);
			i = i + 2;
		} while (i <= 10);
		
		
    // Muestra por pantalla la frase "Hola mundo :-)"
		System.out.println("¡¡Hola mundo!! :-)"); 
    // Muestra por pantalla la frase "Primera clase de POO1"
		System.out.println("Primera clase de POO1");
	}

}
