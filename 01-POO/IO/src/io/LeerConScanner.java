package io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.Arrays;
import java.util.Locale;

public class LeerConScanner {

	/*
	 * este programa lee un archivo con el siguiente formato: primera linea un
	 * entero n que indica la cantidad de numeros siguientes que debo leer seguido
	 * por n numeros tipo double Ver mas ejemplos de Scanner en
	 * http://www.redeszone.net/2012/02/20/curso-de-java-entrada-y-salida-con-
	 * ficheros-clase-scanner/
	 */

	public static void main(String[] args) throws FileNotFoundException {

		File archivo = new File("arch/datos.in");
		Scanner sc = new Scanner(archivo);
		int contadorYs = 0;
		
		while (sc.hasNext()) {
			String linea = sc.nextLine();
			String[] array = linea.split(" ");
			//System.out.println(Arrays.toString(array));
			for (String str : array) {
				if (str.equalsIgnoreCase("y")) {
					contadorYs++;
				}
			}
		}
		
		System.out.println("Cantidad de Ys = "+ contadorYs);
		
		sc.close();
	}

}
/*
 * 
 * 5 
 * 3.2 
 * 3 
 * 4 
 * -1 
 * -2E4
 * 
 */
