package personas;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import static personas.ProcesadorDePersonas.*;

public class App {
	public static void main(String[] args) throws FileNotFoundException {
		
		String archivo = "arch/personas.in";
		
		LinkedList<Persona> personas = getPersonas(archivo);
		LinkedList<Persona> personasMayores = getPersonasMayoresAEdad(personas, 35);
		System.out.println(personasMayores);
		
		System.out.println(getEdadPromedio(personas));
	}
}
