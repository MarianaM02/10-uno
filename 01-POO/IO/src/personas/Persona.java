package personas;

import java.util.Objects;


public class Persona {

	private String dni;
	private String apellido;
	private int edad;
	
	public Persona(String dni, String apellido, int edad) {
		super();
		this.dni = dni;
		this.apellido = apellido;
		this.edad = edad;
	}

	public int getEdad() {
		return edad;
	}

	
	
	@Override
	public int hashCode() {
		return Objects.hash(dni);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		return Objects.equals(dni, other.dni);
	}

	@Override
	public String toString() {
		return dni + ";" + apellido + ";" + edad;
	}
	
}
