package personas;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class ProcesadorDePersonas {

	/*
	 * 1. Implementar un m�todo est�tico getPersonas que reciba el nombre de un
	 * archivo y devuelva un objeto LinkedList<Persona> con personas que fueron
	 * le�das del archivo de texto con formato "dni apellido edad".
	 */
	public static LinkedList<Persona> getPersonas(String archivo) throws FileNotFoundException {
		LinkedList<Persona> personas = new LinkedList<Persona>();
		Scanner sc = new Scanner(new File(archivo));

		String datos;
		String[] arrPer;
		String dni;
		String apellido;
		int edad;
		Persona per;

		while (sc.hasNext()) {
			datos = sc.nextLine();
			arrPer = datos.split(" ");

			dni = arrPer[0];
			apellido = arrPer[1];
			edad = Integer.parseInt(arrPer[2]);

			per = new Persona(dni, apellido, edad);
			personas.add(per);

		}
		sc.close();
		System.out.println(personas);
		return personas;
	}

	/*
	 * 2. Implementar un m�todo est�tico getPersonasMayoresAEdad que reciba un
	 * objeto LinkedList<Persona> y una edad y devuelva otro objeto
	 * LinkedList<Persona> con las personas cuyas edades son mayores a esa edad.
	 */
	public static LinkedList<Persona> getPersonasMayoresAEdad(LinkedList<Persona> personas, int edad) {
		LinkedList<Persona> mayores = new LinkedList<Persona>();
		for (Persona p : personas) {
			if (esPersonaMayorA((int)edad, p)) {
				mayores.add(p);
			}
		}
		return mayores;
	}

	private static boolean esPersonaMayorA(int edad, Persona p) {
		return p.getEdad() >= edad;
	}

	/*
	 * 3. Implementar un m�todo que devuelva la edad promedio de la muestra de
	 * personas.
	 */
	public static double getEdadPromedio(LinkedList<Persona> personas) {
		int suma = 0;
		for (Persona p : personas) {
			suma += p.getEdad();
		}
		int promedio = suma / personas.size();
		return promedio;
	}

	/*
	 * 4. Implementar un m�todo que devuelva la cantidad de personas cuya edad est�
	 * por encima de la edad promedio.
	 */
	public static int getCantPersonasMayoresPromedio(LinkedList<Persona> personas) {
		int cont = 0;
		double promedio = getEdadPromedio(personas);
		for (Persona p : personas) {
			if (esPersonaMayorA((int)promedio, p))
				cont++;
		}
		return cont;
	}

	/*
	 * 5. �Cual es la persona de mayor edad?, si hubiera varias, mostrarlas a todas.
	 */
	/*
	 * 6. �Cual es la persona de menor edad?, si hubiera varias, mostrarlas a todas.
	 */
	/*
	 * 8. Se debe poder listar a las personas ordenadas por dni, por apellido y por
	 * edad (de mayor a menor)
	 */

	public static void escribirArchivo(LinkedList<Persona> personas, String nombreArch) throws IOException {
		FileWriter archivo = new FileWriter("arch/" + nombreArch + ".out");
		PrintWriter salida = new PrintWriter(archivo);

		for (Persona p : personas) {
			salida.println(p);
		}

		salida.close();
	}

}
