package orquesta;

public class App {
	public static void main(String[] args) {
		
		//Crear una orquesta de 10 instrumentos
		
		Instrumento[] conjuntoDeCamara = { 
				new Cuerda("guitarra", 11900, false), 
				new Cuerda("violin", 7308, false),
				new Cuerda("chelo", 220999, false), 
				new Cuerda("contrabajo", 270299, false),
				new Viento("flauta traversa", 37971, false), 
				new Viento("oboe", 291898, false),
				new Viento("trompeta", 26800, true), 
				new Percusion("timbal", 16500, false),
				new Percusion("metalofon", 16094, true), 
				new Percusion("congas", 71531, false) };

		// Implementar el código necesario para que todos los instrumentos 
		// que componen la orquesta se identifiquen y se afinen.
		
		System.out.println("Presntación del Conjunto:");
		for (Instrumento instr : conjuntoDeCamara) {
			System.out.println(instr.queInstrumentoEres());
		}

		System.out.println("\nAfinacion:");
		for (Instrumento instr : conjuntoDeCamara) {
			instr.afinar();
		}
	}
}
