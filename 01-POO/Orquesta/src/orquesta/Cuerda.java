package orquesta;

public class Cuerda extends Instrumento {

	public Cuerda(String nombre, double precio, boolean esMetalico) {
		super(nombre, precio, esMetalico);
	}

	@Override
	protected void afinar() {
		String[] sonido = {"♪♭♩♯♫♮♬", "**Boing!**","**Twang!**"};
		int i = (int) (Math.random()*sonido.length);
		System.out.println(sonido[i]);
	}

	@Override
	public String queInstrumentoEres() {
		return super.getNombre().toUpperCase() 
				+ ": Instrumento de Cuerda";
	}

}
