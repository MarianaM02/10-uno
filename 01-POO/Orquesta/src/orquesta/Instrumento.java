package orquesta;

public abstract class Instrumento implements Identificable {

	private String nombre;
	private double precio;
	private boolean esMetalico;

	protected Instrumento(String nombre, double precio, boolean esMetalico) {
		this.nombre = nombre;
		this.precio = precio;
		this.esMetalico = esMetalico;
	}

	protected String getNombre() {
		return nombre;
	}

	protected double getPrecio() {
		return precio;
	}

	protected boolean isMetalico() {
		return esMetalico;
	}
	
	/**
	 * post: Cómo está sonando el instrumento?
	 */
	protected abstract void afinar();

}
