package orquesta;

public class Percusion extends Instrumento {

	public Percusion(String nombre, double precio, boolean esMetalico) {
		super(nombre, precio, esMetalico);
	}

	@Override
	protected void afinar() {
		String[] sonido = {"♬♬♪♪♩", "**Badumtss!**","**Bumpapum!**"};
		int i = (int) (Math.random()*sonido.length);
		System.out.println(sonido[i]);
	}

	@Override
	public String queInstrumentoEres() {
		return super.getNombre().toUpperCase() 
				+ ": Instrumento de Percusión";
	}

}
