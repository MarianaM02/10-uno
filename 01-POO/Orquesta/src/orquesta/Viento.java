package orquesta;

public class Viento extends Instrumento {

	public Viento(String nombre, double precio, boolean esMetalico) {
		super(nombre, precio, esMetalico);
	}

	@Override
	public void afinar() {
		String[] sonido = {"♪♭♩♯♫♮♬", "**Doot doot!**","**Pshhhht!**"};
		int i = (int) (Math.random()*sonido.length);
		System.out.println(sonido[i]);
	}
	/**
	 * post: Devuelve si es un instrumento de Metal
	 * 
	 */
	public boolean esDeMetal() {
		return super.isMetalico();
	}

	@Override
	public String queInstrumentoEres() {
		return super.getNombre().toUpperCase() 
				+ ": Instrumento de Viento"  
				+ ( esDeMetal() ? " de Metal" : "");
	}

}
