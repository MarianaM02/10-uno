package hotel;

public class Habitacion {
	
	public static final int MAX_HUESPEDES = 8;
	private int cantMayores;
	private int cantMenores;
	
	public int getCantMayores() {
		return cantMayores;
	}
	
	public int getCantMenores() {
		return cantMenores;
	}
	
	public boolean ocupar(int mayores, int menores) {
		if (mayores < 1 || menores < 0) {
			throw new Error("Valores inv�lidos: Deben ser valores "
					+ "positivos y debe haber al menos un Mayor");
		}
		if ((mayores+menores) > MAX_HUESPEDES) {
			throw new Error("Valores inv�lidos: El m�ximo de "
					+ "Hu�spedes por habitaci�n es "+ MAX_HUESPEDES);
		}
		
		if (estaVacia()) {
			this.cantMayores = mayores;
			this.cantMenores = menores;
			return true;
		}
		return false;
		
	}
	
	public int calcularTotal() {
		return cantMayores + cantMenores;
	}
	
	public boolean estaVacia() {
		return calcularTotal() == 0;
	}
	
	@Override
	public String toString() {
		return "\nHabitacion [Mayores=" + cantMayores + ", Menores=" + cantMenores + "]";
	}
	
	
}
