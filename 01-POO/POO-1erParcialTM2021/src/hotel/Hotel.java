package hotel;

import java.util.Arrays;

public class Hotel {
	private Habitacion[] habitaciones;
	
	public Hotel(int cantHabitaciones) {
		this.habitaciones = new Habitacion[cantHabitaciones];
		
		for (int i=0; i<habitaciones.length; i++) {
			habitaciones[i] = new Habitacion();
		}
	}
	
	public Habitacion[] getHabitaciones() {
		return habitaciones;
	}

	/**
	 * post: Ocupar una habitaci�n disponible indicando 
	 * la cantidad de personas mayores y menores 
	 * (m�ximo 8 en total) que la ocupan.
	 * 
	 * @param mayores: Cant de personas adultas
	 * @param menores: Cant de personas menores de edad
	 */
	public void ocuparHabitacionCon(int mayores, int menores) {
		if (!hayVacantes()) {
			throw new Error("No hay m�s vacantes!");
		}
		for(Habitacion h : habitaciones) {
			if (h.ocupar(mayores, menores)) {
				break;
			}
		}
	}
	
	/**
	 * post: Devuelve la cantidad total de personas que 
	 * ocupan todas las habitaciones del hotel.
	 */
	public int contarTotalHuespedes() {
		int totalHuespedes = 0;
		for(Habitacion h : habitaciones) {
			totalHuespedes += h.calcularTotal();
		}
		return totalHuespedes;
	}
	
	/**
	 * post: Devuelve la cantidad de habitaciones que 
	 * est�n ocupadas por tantos mayores como los indicados por par�metro.
	 * 
	 * @param mayores: Valor a comparar (cantidad de personas mayores)
	 */
	public int contarHabitacionesCon(int mayores) {
		int cantHabitaciones = 0;
		for(Habitacion h : habitaciones) {
			int n = h.getCantMayores();
			if (n == mayores) {
				cantHabitaciones++;
			}
		}
		return cantHabitaciones;
	}
	
	/**
	 * post: Devolver un arreglo de enteros de longitud 9, 
	 * tal que en la posici�n i del arreglo se guarde la 
	 * cantidad de habitaciones con i personas.
	 */
	public int[] contarHabitacionesPorCantHuespedes() {
		int[] arr = new int[9];
		for(Habitacion h : habitaciones) {
			int i = h.calcularTotal();
			arr[i]++;
		}
		return arr;
	}
	
	public boolean hayVacantes() {
		return habitaciones[habitaciones.length-1].estaVacia();
	}
	

	@Override
	public String toString() {
		return "Hotel [habitaciones=" + Arrays.toString(habitaciones) + "]";
	}
}
