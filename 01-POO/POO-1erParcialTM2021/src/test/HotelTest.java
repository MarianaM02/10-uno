package test;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import hotel.Habitacion;
import hotel.Hotel;

public class HotelTest {

	Hotel hotel;
	
	@Before
	public void setUp() throws Exception {
		hotel = new Hotel(6);
	}

	@Test
	public void crearHotelTest() {
		assertNotNull(hotel);
		for (Habitacion h : hotel.getHabitaciones()) {
			assertEquals(h.getCantMayores(), 0);
			assertEquals(h.getCantMenores(), 0);
		}
	}
	
	@Test
	public void ocuparHabitacionTest() {
		hotel.ocuparHabitacionCon(2, 3);
		hotel.ocuparHabitacionCon(6, 2);
		Habitacion[] lista = hotel.getHabitaciones();
		assertEquals(lista[0].getCantMayores(), 2);
		assertEquals(lista[0].getCantMenores(), 3);
		assertEquals(lista[1].getCantMayores(), 6);
		assertEquals(lista[1].getCantMenores(), 2);
	}
	
	@Test
	public void contarTotalHuespedes() {
		int obtenido = hotel.contarTotalHuespedes();
		int esperado = 0;
		assertEquals(obtenido, esperado);
		hotel.ocuparHabitacionCon(2, 3);
		hotel.ocuparHabitacionCon(6, 2);
		obtenido = hotel.contarTotalHuespedes();
		esperado = 13;
		assertEquals(obtenido, esperado);
	}
	
	@Test
	public void contarHabitacionesConTest() {
		hotel.ocuparHabitacionCon(2, 3);
		hotel.ocuparHabitacionCon(6, 2);
		int obtenido = hotel.contarHabitacionesCon(2);
		int esperado = 1;
		assertEquals(obtenido, esperado);
		
		obtenido = hotel.contarHabitacionesCon(0);
		esperado = 4;
		assertEquals(obtenido, esperado);
		
		obtenido = hotel.contarHabitacionesCon(5);
		esperado = 0;
		assertEquals(obtenido, esperado);
	}
	
	@Test
	public void contarHabPorCantHuespedes() {
		int[] arrObtenido = hotel.contarHabitacionesPorCantHuespedes();
		int[] arrEsperado = {6,0,0,0,0,0,0,0,0};
		
		assertArrayEquals(arrEsperado, arrObtenido);
		
		hotel.ocuparHabitacionCon(2, 3);
		hotel.ocuparHabitacionCon(6, 2);
		
		int[] arrObtenido2 = hotel.contarHabitacionesPorCantHuespedes();
		int[] arrEsperado2 = {4,0,0,0,0,1,0,0,1};
		
		assertArrayEquals(arrEsperado2, arrObtenido2);
	}
	
	
	@Test (expected = Error.class)
	public void hotelLlenoTest() {
		hotel.ocuparHabitacionCon(2, 3);
		hotel.ocuparHabitacionCon(6, 2);
		hotel.ocuparHabitacionCon(2, 3);
		hotel.ocuparHabitacionCon(6, 2);
		hotel.ocuparHabitacionCon(2, 3);
		hotel.ocuparHabitacionCon(2, 3);
		hotel.ocuparHabitacionCon(2, 3);
	}
	
	@Test (expected = Error.class)
	public void superarMaximoHuespedesHabTest() {
		hotel.ocuparHabitacionCon(7, 5);
	}
	
	@Test (expected = Error.class)
	public void minimoMayoresTest() {
		hotel.ocuparHabitacionCon(0, 4);
	}
	
	@Test (expected = Error.class)
	public void cantNegativasTest() {
		hotel.ocuparHabitacionCon(2, -4);
	}
	
	@Test
	public void toStringTest() {
		System.out.println(hotel);
	}
	
	

}
