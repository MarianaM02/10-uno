package maze;

public class Maze {
	private int rows;
	private int columns;

	public Maze(int rows, int columns) {
		this.columns = columns;
		this.rows = rows;

	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public boolean isPacManAt(int n, int m) {
		return false;
	}

	public boolean isPacManLookingDown() {
		return false;
	}

	public boolean isPacManLookingUp() {
		return false;
	}

	public boolean isPacManLookingLeft() {
		return false;
	}

	public boolean isPacManLookingRight() {
		return false;
	}

	public boolean isEmptyAt(int n, int m) {
		return false;
	}

	public void pacManDown() {

	}

	public void pacManUp() {

	}

	public void pacManLeft() {

	}

	public void pacManRight() {

	}

	public void tick() {

	}

	public void print() {

	}
}
