package ar.edu.uno.poo1.ejercicioentregable;

public enum Especialidad {
	MEDICO_CLINICO("Médico clínico"), 
	OFTAMOLOGIA("Oftamología"),
	ODONTOLOGIA("Odontología");
	
	// Atributo
	private String nombre;
	
	// Constructor privado
	private Especialidad(String nombre) {
		this.nombre = nombre;
	}
	
	// Getter
	public String getNombre() {
		return this.nombre;
	}
}
