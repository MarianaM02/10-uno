package ar.edu.uno.poo1.ejercicioentregable;

public class TurnoMedico {
	private String paciente;
	private Especialidad especialidad;
	
	public TurnoMedico(String paciente, Especialidad especialidad) {
		this.paciente = paciente;
		this.especialidad = especialidad;
	}
	
	public String getPaciente() {
		return this.paciente;
	}
	
	public Especialidad getEspecialidad() {
		return this.especialidad;
	}
	
	public void setEspecialidad(Especialidad nuevaEspecialidad) {
		this.especialidad = nuevaEspecialidad;
	}
	
	public boolean esMedicoClinico() {
		if (this.getEspecialidad() == Especialidad.MEDICO_CLINICO) {
			return true;
		}
		
		return false;
	}
	
	public String resumenTurno() {
		if (this.esMedicoClinico()) {
			return this.getPaciente() + "\n" + this.getEspecialidad().getNombre();
		}
		
		return "El turno no corresponde a la especialidad médico clínico";
	}
}
