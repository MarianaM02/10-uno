package ar.edu.uno.poo1.ejercicioentregable;

public class TurnoMedicoTest {
	public static void main(String[] args) {
		TurnoMedico turno = new TurnoMedico("Lucas", Especialidad.MEDICO_CLINICO);
		
		/*
		 * turno -> objeto
		 * turno.getEspecialidad() -> Especialidad
		 * turno.getEspecialidad().getNombre() -> String
		 */
		//System.out.println("Especialidad: " + turno.getEspecialidad().getNombre());
		
		turno.setEspecialidad(Especialidad.OFTAMOLOGIA);
		System.out.println(turno.resumenTurno());
	}
}
