/*
 * CONSIGNA (EJEMPLO HERENCIA):
 * Todas las personas se caracterizan por su DNI y su nombre.
 * 		Además, pueden comer y dormir (informar de esto por pantalla)
 * Un trabajador es una persona que además tiene salario y antigüedad.
 * 		Los trabajadores pueden trabajar (informar de esto por pantalla)
 * Un estudiante es una persona con promedio, que puede estudiar (informar de esto por pantalla)
 */

package ar.edu.uno.poo1.herencia;

public class Persona {
	// Atributos
	private int dni;
	private String nombre;
	
	// Constructor
	public Persona(int dni, String nombre) {
		this.dni = dni;
		this.nombre = nombre;
	}
	
	// Métodos
	public int getDni() {
		return this.dni;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	// Nivel de acceso protected (protegido)
	// El elemento será visible desde la clase donde se lo definió
	// y desde sus sub-clases
	protected void dormir() {
		System.out.println("PERSONA durmiendo");
	}
	
	public void comer() {
		System.out.println("PERSONA comiendo");
	}	
}
