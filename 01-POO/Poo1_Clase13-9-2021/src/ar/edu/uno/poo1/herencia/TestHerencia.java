package ar.edu.uno.poo1.herencia;

public class TestHerencia {
	public static void main(String[] args) {
		Trabajador t = new Trabajador(11222333, "Lucas", 10580.55, 4);
//		t.comer();
//		t.trabajar();
//		t.dormir();
		
		TrabajadorTemporario tt = new TrabajadorTemporario(11222333, "Lucas", 10580.55, 4, 3);
//		tt.trabajar();
		
		tt.comer();
		
//		tt.dormir();
	}
}
