package ar.edu.uno.poo1.herencia;

public class Trabajador extends Persona {
	// Atributos
	private double salario;
	private int antiguedad;
	
	public Trabajador(int dni, String nombre, double salario, int antiguedad) {
		// "super" hace referencia a la superclase
		
		// Invocamos al constructor de la super-clase (Persona)
		super(dni, nombre);
		
		this.salario = salario;
		this.antiguedad = antiguedad;
	}
	
	// Métodos
	public void trabajar() {
		System.out.println("TRABAJANDO");
	}
	
}

