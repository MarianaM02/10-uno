package ar.edu.uno.poo1.herencia;

/*
 * CONSIGNA: Un trabajador temporario es un trabajador, que además
 * posee un contrato con una duración específica.
 * 
 * Tomar en cuenta que un trabajador temporario puede comer,
 * pero de una forma diferente a todas las personas.
 */
public class TrabajadorTemporario extends Trabajador {
	// Atributos
	private int duracionContrato;
	
	// Constructor
	public TrabajadorTemporario(int dni, String nombre, double salario, int antiguedad, int duracionContrato) {
		super(dni, nombre, salario, antiguedad);
		this.duracionContrato = duracionContrato;
	}
	
	// Métodos
	
	// Sobreescribimos (over-writing) el comportamiento comer()
	//		heredado de Persona
	@Override
	public void comer() {
		System.out.println("TRABAJADOR TEMPORARIO comiendo");
	}
	
}
