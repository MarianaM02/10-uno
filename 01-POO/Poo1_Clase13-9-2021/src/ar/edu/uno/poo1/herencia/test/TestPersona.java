package ar.edu.uno.poo1.herencia.test;

import ar.edu.uno.poo1.herencia.Persona;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestPersona {
	@Test
	public void testCrearUnaPersona() {
		Persona p = new Persona(11222333, "Lucas");
		assertNotNull(p);
	}
	
	@Test
	public void testGetterNumeroDeDocumento() {
		Persona p = new Persona(11222333, "Lucas");
		int valorEsperado = 11222333;
		int valorObtenido = p.getDni();
		assertEquals(valorEsperado, valorObtenido);
	}
}
