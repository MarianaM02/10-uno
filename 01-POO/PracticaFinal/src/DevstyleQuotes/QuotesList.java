package DevstyleQuotes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Scanner;

public class QuotesList {
	private ArrayList<String> quotes;

	public QuotesList() throws FileNotFoundException {
		this.quotes = new ArrayList<String>();
		leerLista();
	}

	private void leerLista() throws FileNotFoundException {
		File arch = new File("quotes.txt");
		Scanner sc = new Scanner(arch);
		String linea;
		while (sc.hasNext()) {
			linea = sc.nextLine();
			this.quotes.add(linea);
		}
		sc.close();
	}

	public ArrayList<String> getQuotes() {
		return quotes;
	}
//////////////////////////////////////////////////////////////////////////
	public static void main(String[] args) throws IOException {
		QuotesList ql = new QuotesList();

		ql.getQuotes().forEach(q -> System.out.println(q));
    
	}
//////////////////////////////////////////////////////////////////////////
}
