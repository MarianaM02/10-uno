package EmpleadosOrdenados;

public class App {
	public static void main(String[] args) {
		Empresa empre = new Empresa("IWP S.R.L.");
		empre.contratar(new Empleado("Mariana","Madeira",36798802));
		empre.contratar(new Empleado("gdgdjskjc","dknlkasnck",764915615));
		empre.contratar(new Empleado("lfbca","ralkjc",65015974));
		empre.contratar(new Empleado("jfknvnvnn","hfvskdmd",3850750));
		empre.contratar(new Empleado("lfbca","ralkjc",65015974));
		empre.contratar(new Empleado("gdgdjskjc","dknlkasnck",764915615));
		empre.contratar(new Empleado("Mariana","Madeira",36798802));
		
		System.out.println(empre);

		empre.mostrarEmpleadosRepetidos();
		
		empre.ordenarEmpleadosXDNI();
		
		System.out.println(empre);
		
	}
}
