package EmpleadosOrdenados;

import java.util.Comparator;

public class DNIComparator implements Comparator<Empleado> {

	@Override
	public int compare(Empleado o1, Empleado o2) {
		return Integer.compare(o1.getDni(), o2.getDni());
	}

}
