package EmpleadosOrdenados;

import java.util.Objects;

public class Empleado {
	private String nombre;
	private String apellido;
	private int dni;
	
	public Empleado(String nombre, String apellido, int dni) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public int getDni() {
		return dni;
	}

	@Override
	public int hashCode() {
		return Objects.hash(apellido, dni, nombre);
	}

	
	
	@Override
	public String toString() {
		return "[" + nombre + " " + apellido + ", dni=" + dni + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empleado other = (Empleado) obj;
		return Objects.equals(apellido, other.apellido) && dni == other.dni && Objects.equals(nombre, other.nombre);
	}
	
}
