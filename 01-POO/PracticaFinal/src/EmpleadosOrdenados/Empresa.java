package EmpleadosOrdenados;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;

public class Empresa {
	private String razonSocial;
	private LinkedList<Empleado> listaEmpleados;
	
	public Empresa(String razonSocial) {
		this.razonSocial = razonSocial;
		this.listaEmpleados = new LinkedList<Empleado>();
	}
	/**
	 * Ej 1 Ordenar en forma ascendente, seg�n su numero de dni, una lista de empleados.
	 */
	public void ordenarEmpleadosXDNI() {
		Collections.sort(listaEmpleados, new DNIComparator());
	}
	
	
	/**
	 * E2 Agregue un m�todo que devuelva los elementos repetidos.
	 */
	public void mostrarEmpleadosRepetidos() {
		HashSet<Empleado> setEmpleados = new HashSet<Empleado>();
		
		System.out.println("EMPLEADOS REPETIDOS");
		for(Empleado e : listaEmpleados) {
			if(!setEmpleados.add(e))
				System.out.println(e);
		}
	}
	
	public void contratar(Empleado e) {
		listaEmpleados.add(e);
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public LinkedList<Empleado> getListaEmpleados() {
		return listaEmpleados;
	}

	@Override
	public String toString() {
		return razonSocial + "\n" + listaEmpleados;
	}
	
}
