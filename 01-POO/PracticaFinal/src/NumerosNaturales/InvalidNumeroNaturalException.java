package NumerosNaturales;

@SuppressWarnings("serial")
public class InvalidNumeroNaturalException extends RuntimeException{

	public InvalidNumeroNaturalException(String message) {
		super(message);
	}

}
