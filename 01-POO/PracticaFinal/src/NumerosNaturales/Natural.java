package NumerosNaturales;

public class Natural implements Comparable<Natural>{ 
	
	private int valor;

	public Natural(float valor) throws InvalidNumeroNaturalException {
		if(valor <= 0 || valor % (int)valor != 0 )
			throw new InvalidNumeroNaturalException(valor + " no es n�mero natural");
		this.valor = (int)valor;
	}

	public int getValor() {
		return valor;
	}

	@Override
	public int compareTo(Natural o) {
		return Integer.compare(this.valor, o.getValor());
	}
	
	
	
	
}
