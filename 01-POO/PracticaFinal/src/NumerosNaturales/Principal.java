package NumerosNaturales;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Scanner;

public class Principal {

	public static LinkedList<Natural> getNaturales(String ruta) throws FileNotFoundException {
		LinkedList<Natural> listaNumeros = new LinkedList<Natural>();
		File archivo = new File(ruta);
		Scanner sc = new Scanner(archivo);
		sc.useLocale(Locale.ENGLISH);
		while (sc.hasNext()) {
			try {
				listaNumeros.add(new Natural(sc.nextFloat()));
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}

		sc.close();
		return listaNumeros;
	}

	public static void listarOrdenadoxJuan(LinkedList<Natural> lista, String ruta) throws IOException {
		File archivo = new File(ruta);
		FileWriter fw = new FileWriter(archivo);
		PrintWriter salida = new PrintWriter(fw);
		
		LinkedList<Natural> pares = new LinkedList<Natural>();
		LinkedList<Natural> impares = new LinkedList<Natural>();

		for (Natural n : lista) {
			if (n.getValor() % 2 == 0)
				pares.add(n);
			else
				impares.add(n);
		}
		Collections.sort(pares);
		Collections.sort(impares);
		
		pares.forEach(n -> salida.println(n.getValor()));
		impares.forEach(n -> salida.println(n.getValor()));
		salida.close();
	}

	public static void main(String[] args) throws FileNotFoundException, IOException {
		listarOrdenadoxJuan(getNaturales("numeros.in"), "salida.in");
	}
}
