package problems;

public class Ej001 {
	/*
	 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we
	 * get 3, 5, 6 and 9. The sum of these multiples is 23.
	 * 
	 * Find the sum of all the multiples of 3 or 5 below 1000.
	 */
	public static void main(String[] args) {
		final int TOPE = 1000;
		int resultado = 0;
		for (int i = 0; i < TOPE; i++) {
			if (i % 3 == 0 || i % 5 == 0) {
				resultado += i;
			}
		}
		System.out.println(resultado);
	}
}
