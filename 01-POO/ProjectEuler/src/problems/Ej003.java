package problems;

public class Ej003 {
	/* 
	 * The prime factors of 13195 are 5, 7, 13 and 29.
	 * What is the largest prime factor of the number 600851475143 ? 
	 */
	public static void main(String[] args) {
		System.out.println("Ejemplo:");
		encontrarFactores(13195);
		System.out.println("El numero a factorear:");
		encontrarFactores(600851475143L);
		
		System.out.println(Math.sqrt(87625999));
		
		System.out.println("Ejemplo:");
		encontrarFactores2(600851475143L);
	}

	private static void encontrarFactores(long numero) {
		long divisor = 2;
		while (divisor <= numero) {
			if (esNumeroPrimo(divisor) && esDivisor(numero, divisor)) {
				System.out.printf("%12d|%4d\n", numero, divisor);
				numero = (numero / divisor);
				divisor = 2;
			} else {
				divisor++;
			}
		}
		System.out.printf("%12d|\n\n", 1);
	}
	private static boolean encontrarFactores2(long numero) {
		long n;
		if (esNumeroPrimo(numero))
			n = numero;
		else
			n = (long)Math.sqrt(numero);
		
		for (long divisor = n; divisor > 1; divisor--) {
			if (esNumeroPrimo(divisor) && esDivisor(numero, divisor)) {
				System.out.printf("%12d|%4d\n", numero, divisor);
				encontrarFactores(numero / divisor);
				return true;
			}
		}
		System.out.printf("%12d|\n\n", 1);
		return false;
	}
	

	private static boolean esNumeroPrimo(long numero) {
		if (numero < 2)
			return false;
		for (long divisor = 2; divisor <= (long) Math.sqrt(numero); divisor++) {
			if (esDivisor(numero, divisor))
				return false;
		}
		return true;
	}

	private static boolean esDivisor(long numero, long divisor) {
		return numero % divisor == 0;
	}
}
