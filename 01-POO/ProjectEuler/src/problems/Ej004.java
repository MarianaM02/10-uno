package problems;

public class Ej004 {
	/*
	 * A palindromic number reads the same both ways. The largest palindrome made
	 * from the product of two 2-digit numbers is 9009 = 91 × 99.
	 * 
	 * Find the largest palindrome made from the product of two 3-digit numbers.
	 */
	public static void main(String[] args) {
		mayorPalindromo();
	}

	public static void mayorPalindromo() {
		int mayor = 0;
		int iMayor = 0, jMayor = 0;
		for (int i = 999; i > 100; i--) {
			for (int j = 999; j > 100; j--) {
				int prod = i * j;
				if (esPalindromo(prod) && prod > mayor) {
					mayor = prod;
					iMayor = i;
					jMayor = j;
				}
			}
		}
		System.out.printf("%d x %d = %s", iMayor, jMayor, mayor);
	}

	public static boolean esPalindromo(int n) {
		String s = Integer.toString(n);
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != s.charAt(s.length() - i - 1))
				return false;
		}
		return true;
	}

	public static int cuantosDigitos(int n) {
		int cont = 0;
		int divisor = 1;
		while ((n / divisor) != 0) {
			divisor *= 10;
			cont++;
		}
		return cont;
	}
}
