package problems;

import java.util.Arrays;

public class Ej005 {
	/*
	 * 2520 is the smallest number that can be divided by each of the numbers from 1
	 * to 10 without any remainder.
	 * 
	 * What is the smallest positive number that is evenly divisible by all of the
	 * numbers from 1 to 20?
	 */
	public static void main(String[] args) {
		int[] factoresRango = encontrarFactores();
		System.out.println(Arrays.toString(factoresRango));
		int productoFactores = multiplicarIndices(factoresRango);
		System.out.println(productoFactores);
		long f = factorial(20);

		for (long i = 2520; i < f; i++) {
			boolean bandera = true;
			for (long j = 1; j <= 20; j++) {
				if (!esDivisor(i, j)) {
					bandera = false;
					break;
				}
			}
			if (bandera) {
				System.out.println(i);
				break;
			}
		}
		System.out.println("Termino");
	}

	public static int multiplicarIndices(int[] arr) {
		int prod = 1;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != 0)
				prod *= i;
		}
		return prod;
	}

	private static int[] encontrarFactores() {
		int[] listaFactores = new int[20];

		for (int i = 0; i <= 20; i++) {
			long numero = i;
			long divisor = numero;
			while (divisor > 1) {
				if (esNumeroPrimo(divisor) && esDivisor(numero, divisor)) {
					// System.out.printf("%4d|%3d\n", numero, divisor);
					listaFactores[(int) divisor] = 1;
					numero = divisor = (numero / divisor);
				} else {
					divisor--;
				}
			}
			// System.out.printf("%4d|\n\n", 1);
		}
		return listaFactores;
	}

	public static long factorial(long n) {
		if (n > 1)
			return n * factorial(n - 1);
		else
			return n;
	}

	private static boolean esNumeroPrimo(long numero) {
		if (numero < 2)
			return false;
		for (long divisor = 2; divisor < numero; divisor++) {
			if (esDivisor(numero, divisor))
				return false;
		}
		return true;
	}

	private static boolean esDivisor(long numero, long divisor) {
		return numero % divisor == 0;
	}
}
