package problems;

public class Ej006 {
	/*
	 * The sum of the squares of the first ten natural numbers is,
	 * 		1²+2²+...+10² = 385
	 * The square of the sum of the first ten natural numbers is,
	 * 		(1+2+...+10)² = 3025
	 * Hence the difference between the sum of the squares of the first ten natural
	 * numbers and the square of the sum is
	 * 		3025-385 = 2640
	 * 
	 * Find the difference between the sum of the squares of the first one hundred
	 * natural numbers and the square of the sum.
	 */
	public static void main(String[] args) {
		long r1 = cuadradoSumas(100);
		System.out.println(r1);
		long r2 = sumaCuadrados(100);
		System.out.println(r2);
		System.out.println(r1 - r2);

	}

	public static long sumaCuadrados(long n) {
		long sumatoria = 0;
		for (int i = 1; i <= n; i++) {
			sumatoria += (long) Math.pow(i, 2);
		}
		return sumatoria;
	}

	public static long cuadradoSumas(long n) {
		long sumatoria = 0;
		for (int i = 1; i <= n; i++) {
			sumatoria += i;
		}
		long resultado = (long) Math.pow(sumatoria, 2);
		return resultado;
	}
}
