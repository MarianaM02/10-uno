package problems;

public class Ej007 {
	/*
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 * 
	 * What is the 10 001st prime number?
	 */
	public static void main(String[] args) {

		listaNumerosPrimos(10001);

	}

	private static void listaNumerosPrimos(int limite) {
		int cantPrimos = 0;
		// se considera que el primer numero primo es 2
		int contador = 2;

		while (cantPrimos != limite) {
			if (esNumeroPrimo(contador)) {
				System.out.println(contador);
				cantPrimos++;
			}
			contador++;
		}
	}

	private static boolean esNumeroPrimo(int numero) {
		if (numero < 2)
			return false;
		for (int divisor = 2; divisor < numero; divisor++) {
			if (numero % divisor == 0)
				return false;
		}
		return true;
	}
}
