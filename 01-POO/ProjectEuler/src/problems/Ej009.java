package problems;

public class Ej009 {
	/*
	 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for
	 * which, a² + b² = c²
	 * 
	 * For example, 3² + 4² = 9 + 16 = 25 = 5².
	 * 
	 * There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find
	 * the product abc.
	 */
	public static void main(String[] args) {
		loop: for (int a = 0; a < 1000; a++) {
			for (int b = 0; b < 1000; b++) {
				for (int c = 0; c < 1000; c++) {
					if (estanOrdenados(a, b, c) && sumanN(a, b, c, 1000) && esPythagoreanTriplet(a, b, c)) {
						System.out.printf("a = %d, b = %d, c = %d\n", a, b, c);
						System.out.printf("producto = %d\n", a * b * c);
						break loop;
					}
				}
			}
		}
	}

	public static boolean esCuadradoPerfecto(int n) {
		return Math.sqrt(n) == (int) Math.sqrt(n);
	}

	public static boolean estanOrdenados(int a, int b, int c) {
		return a < b && b < c;
	}

	public static boolean sumanN(int a, int b, int c, int n) {
		return a + b + c == n;
	}

	public static boolean esPythagoreanTriplet(int a, int b, int c) {
		return Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2);
	}

}
