package problems;

public class Ej010 {
	/*
	 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
	 * 
	 * Find the sum of all the primes below two million.
	 */
	public static void main(String[] args) {
		long sumatoria = 0;
		LibEuler lib = new LibEuler();
		for (long i = 0; i < 2000000; i++) {
			if (lib.esNumeroPrimo(i)) {
				sumatoria += i;
			}
		}
		System.out.println(sumatoria);
	}

}
