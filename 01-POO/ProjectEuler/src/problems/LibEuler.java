package problems;

public class LibEuler {

	public boolean esCuadradoPerfecto(int n) {
		return Math.sqrt(n) == (int) Math.sqrt(n);
	}

	public boolean estanOrdenados3(int a, int b, int c) {
		return a < b && b < c;
	}

	public boolean suman3(int a, int b, int c, int n) {
		return a + b + c == n;
	}

	public boolean esPythagoreanTriplet(int a, int b, int c) {
		return Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2);
	}

	public void encontrarFactores(long numero) {
		long divisor = 2;
		while (divisor <= numero) {
			if (esNumeroPrimo(divisor) && esDivisor(numero, divisor)) {
				System.out.printf("%12d|%4d\n", numero, divisor);
				numero = (numero / divisor);
				divisor = 2;
			} else {
				divisor++;
			}
		}
		System.out.printf("%12d|\n\n", 1);
	}

	public boolean esNumeroPrimo(long numero) {
		if (numero < 2)
			return false;
		for (long divisor = 2; divisor <= (long) Math.sqrt(numero); divisor++) {
			if (esDivisor(numero, divisor))
				return false;
		}
		return true;
	}

	public void listaNumerosPrimos(int limite) {
		int cantPrimos = 0;
		int contador = 2;

		while (cantPrimos != limite) {
			if (esNumeroPrimo(contador)) {
				System.out.println(contador);
				cantPrimos++;
			}
			contador++;
		}
	}

	public boolean esDivisor(long numero, long divisor) {
		return numero % divisor == 0;
	}

	public int fibonacci(int num) {
		if (num < 2)
			return num;
		else
			return fibonacci(num - 1) + fibonacci(num - 2);
	}

	public long sumaCuadrados(long n) {
		long sumatoria = 0;
		for (int i = 1; i <= n; i++) {
			sumatoria += (long) Math.pow(i, 2);
		}
		return sumatoria;
	}

	public long cuadradoSumas(long n) {
		long sumatoria = 0;
		for (int i = 1; i <= n; i++) {
			sumatoria += i;
		}
		long resultado = (long) Math.pow(sumatoria, 2);
		return resultado;
	}

	public long factorial(long n) {
		if (n > 1)
			return n * factorial(n - 1);
		else
			return n;
	}

	public boolean esPalindromo(int n) {
		String s = Integer.toString(n);
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) != s.charAt(s.length() - i - 1))
				return false;
		}
		return true;
	}

	public int cuantosDigitos(int n) {
		int cont = 0;
		int divisor = 1;
		while ((n / divisor) != 0) {
			divisor *= 10;
			cont++;
		}
		return cont;
	}

	public void primerosNumerosNaturales(int limite) {
		for (int i = 0; i < limite; i++) {
			System.out.println(i);
		}
	}

	public void mostrarArregloEnLinea(int[] arreglo) {
		for (int i = 0; i < arreglo.length; i++) {
			System.out.print(arreglo[i] + " ");
		}
		System.out.println();
	}

	public int[] invertirArreglo(int[] arreglo) {
		int[] arrInvertido = new int[arreglo.length];
		int subindice = 0;
		for (int i = arreglo.length - 1; i >= 0; i--) {
			arrInvertido[subindice] = arreglo[i];
			subindice++;
		}
		return arrInvertido;
	}

	public String invertirString(String s) {
		String nuevoString = "";
		for (int i = s.length() - 1; i >= 0; i--) {
			nuevoString += s.charAt(i);
		}
		return nuevoString;
	}

	public void fizzBuzz(int n1, int n2, int limite) {
		for (int i = 1; i < limite; i++) {
			String valor = Integer.toString(i);
			if (i % n1 == 0)
				valor = "Fizz";
			if (i % n2 == 0)
				valor = "Buzz";
			if (i % (n1 * n2) == 0)
				valor = "FizzBuzz";
			System.out.println(valor);
		}
	}
	
	public void serieAcumulacion(int limite) {
		int sumatoria=0;
		for (int i=1; i<=limite; i++) {
			sumatoria += i;
			System.out.printf("%4d | %7d\n",i,sumatoria);
		}
	}
	
	public int serieGauss(int n) {
		return (n*(n+1))/2;
	}
	
	public static String cifraCesar(String s, int n) {
		final String MIN = "abcdefghijklmnñopqrstuvwxyz";
		String nuevoString = "";
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (Character.isLetter(c)) {
				c = Character.toLowerCase(c);
				int p = MIN.indexOf(c);
				int nuevaP = Math.abs((MIN.length() + p + n) % (MIN.length()));
				char nuevoC = MIN.charAt(nuevaP);
				nuevoString += nuevoC;
			} else {
				nuevoString += c;
			}
		}
		return nuevoString;
	}
}
