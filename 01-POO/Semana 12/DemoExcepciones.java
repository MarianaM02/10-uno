package ar.gov.uno.poo1.excepciones_archivos.modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.LinkedList;

public class DemoExcepciones {

	public static void main(String[] args) throws Exception {
		LinkedList<Moneda>  monedas = new LinkedList<Moneda>();
		File file = new File("monedas.txt");
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		String linea = "";
		while ((linea = br.readLine()) != null) {
			String [] split = linea.split(";");
			Moneda moneda = new Moneda(Double.parseDouble(split[0]), split[1]);
			monedas.add(moneda);
		}
		br.close();
		
		HashMap<String,Double> mapaMonetario = new HashMap<String, Double>();
		
		for (Moneda mon : monedas) {
			String codigo = mon.getTipoMoneda().getCodigo();
			double acum = mon.getValor();
			if (mapaMonetario.get(codigo) != null)
				acum += mapaMonetario.get(codigo);
			mapaMonetario.put(codigo, acum);	
		}
		
		for (String clave : mapaMonetario.keySet())
			System.out.println(clave + " = " + mapaMonetario.get(clave));
			
	}

}
