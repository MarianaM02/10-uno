package tarjetaBaja;

public class TarjetaBaja {
	private double boletoColectivo = 1.25;
	private double boletoSubte = 2.5;
	private double saldo;
	private int cantViajesSubte;
	private int cantViajesColectivo;

	/**
	 * post: saldo de la Tarjeta en saldoInicial.
	 */
	public TarjetaBaja(double saldoInicial) {
		this.saldo = saldoInicial;
		this.cantViajesSubte = 0;
		this.cantViajesColectivo = 0;
	}

	public double getSaldo() {
		return this.saldo;
	}

	/**
	 * post: agrega el monto al saldo de la Tarjeta.
	 */
	public void cargar(double monto) {
		if (monto > 0)
			this.saldo += monto;
		else 
			throw new Error("Valor inv�lido! (Valor negativo o nulo)");
	}

	/**
	 * pre : saldo suficiente.
	 * 
	 * post: utiliza 1.25 del saldo para un viaje en colectivo.
	 */
	public void pagarViajeEnColectivo() {
		if (saldo >= boletoColectivo) {
			this.saldo -= boletoColectivo;
			this.cantViajesColectivo++;
		} else {
			throw new Error("Valor inv�lido! (Saldo Insuficiente)");
		}
	}

	/**
	 * pre : saldo suficiente.
	 * 
	 * post: utiliza 2.50 del saldo para un viaje en subte.
	 */
	public void pagarViajeEnSubte() {
		if (saldo >= boletoSubte) {
			this.saldo -= boletoSubte;
			this.cantViajesSubte++;
		} else {
			throw new Error("Valor inv�lido! (Saldo Insuficiente)");
		}
	}

	/**
	 * post: devuelve la cantidad de viajes realizados.
	 */
	public int getViajes() {
		return this.cantViajesSubte + this.cantViajesColectivo;
	}

	/**
	 * post: devuelve la cantidad de viajes en colectivo.
	 */
	public int getViajesEnColectivo() {
		return this.cantViajesColectivo;
	}

	/**
	 * post: devuelve la cantidad de viajes en subte.
	 */
	public int getViajesEnSubte() {
		return this.cantViajesSubte;
	}

	/**
	 * post: devuelve el precio de un boleto de colectivo
	 * 
	 * @return
	
	public double obtenerBoletoColectivo() {
		return boletoColectivo;
	}*/
	
	/**
	 * post: cambia el precio de un boleto de colectivo
	 * 
	 * @param boletoColectivo
	 
	public void cambiarBoletoColectivo(double boletoColectivo) {
		this.boletoColectivo = boletoColectivo;
	}*/

	/**
	 * post: devuelve el precio de un boleto de subte
	 * 
	 * @return
	 
	public double obtenerBoletoSubte() {
		return boletoSubte;
	}*/

	/**
	 * post: cambia el precio de un boleto de subte
	 * 
	 * @param boletoSubte
	 
	public void cambiarBoletoSubte(double boletoSubte) {
		this.boletoSubte = boletoSubte;
	}*/
}