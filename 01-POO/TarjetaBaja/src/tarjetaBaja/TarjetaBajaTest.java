package tarjetaBaja;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TarjetaBajaTest {
	TarjetaBaja tb;
	
	@Before
	public void setUp() throws Exception {
		tb = new TarjetaBaja(500);
	}

	@Test
	public void creoTarjetaTest() {
		assertNotNull(tb);
	}

	@Test
	public void consultarSaldoTest() {
		double esperado = 500;
		double obtenido = tb.getSaldo();
		assertEquals(esperado, obtenido, 0.001);
	}
	
	@Test
	public void consultarViajesColectivoTest() {
		double esperado = 0;
		double obtenido = tb.getViajesEnColectivo();
		assertEquals(esperado, obtenido, 0.001);
	}
	
	@Test
	public void consultarViajesSubteTest() {
		double esperado = 0;
		double obtenido = tb.getViajesEnSubte();
		assertEquals(esperado, obtenido, 0.001);
	}
	
	@Test
	public void viajarColectivoTest() {
		tb.pagarViajeEnColectivo();
		double esperado = 1;
		double obtenido = tb.getViajesEnColectivo();
		assertEquals(esperado, obtenido, 0.001);
	}
	
	@Test
	public void viajarSubteTest() {
		tb.pagarViajeEnSubte();
		double esperado = 1;
		double obtenido = tb.getViajesEnSubte();
		assertEquals(esperado, obtenido, 0.001);
	}
	
	@Test
	public void totalViajesTest() {
		tb.pagarViajeEnColectivo();
		tb.pagarViajeEnSubte();
		double esperado = 2;
		double obtenido = tb.getViajes();
		assertEquals(esperado, obtenido, 0.001);
	}
	
	@Test
	public void cargarTarjetaTest() {
		tb.cargar(200);
		double esperado = 700;
		double obtenido = tb.getSaldo();
		assertEquals(esperado, obtenido, 0.001);
	}
	
	@Test(expected = Error.class)
	public void cargarNegativoTest() {
		tb.cargar(-200);
	}
	
	@Test(expected = Error.class)
	public void saldoInsuficienteColectivoTest() {
		TarjetaBaja tb2 = new TarjetaBaja(1);
		tb2.pagarViajeEnColectivo();
	}
	
	@Test(expected = Error.class)
	public void saldoInsuficienteSubteTest() {
		TarjetaBaja tb2 = new TarjetaBaja(1);
		tb2.pagarViajeEnSubte();
	}

}
