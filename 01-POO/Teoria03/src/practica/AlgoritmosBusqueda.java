package practica;

public class AlgoritmosBusqueda {
	public static void main(String[] args) {

	}

	public static boolean busquedaArregloDesordenado(int[] arr, int n) {
		for (int i = 0; i < arr.length; i++) {
			if (n == arr[i])
				return true;
		}
		return false;
	}

	public static int busquedaBinaria(int[] arr, int v, int l, int r) {
		while (r >= 1) {
			int m = (l + r) / 2;
			if (v == arr[m])
				return m;
			if (v < arr[m])
				r = m - 1;
			else
				l = m + 1;
		}
		return -1;
	}
}
