package practica;

public class CifraCesar {
	public static void main(String[] args) {

		System.out.println(cifraCesar("Hola Mundis!", -2));

	}

	/**
	 * Encripta un mensaje usando el método Cifra César.
	 * 
	 * @param s String a convertir
	 * @param n Lugares que se mueve la cifra
	 * @return
	 */
	public static String cifraCesar(String s, int n) {
		final String MIN = "abcdefghijklmnñopqrstuvwxyz";
		String nuevoString = "";
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (Character.isLetter(c)) {
				c = Character.toLowerCase(c);
				int p = MIN.indexOf(c);
				int nuevaP = Math.abs((MIN.length() + p + n) % (MIN.length()));
				char nuevoC = MIN.charAt(nuevaP);
				nuevoString += nuevoC;
			} else {
				nuevoString += c;
			}
		}
		return nuevoString;
	}
}
