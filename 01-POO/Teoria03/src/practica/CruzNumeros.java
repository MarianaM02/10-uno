package practica;

public class CruzNumeros {
	public static void main(String[] args) {
		cruzNumeros(5);
		cruzNumeros(3);
		cruzNumeros(4);
	}

	/**
	 * Escribe una cruz de numeros siguiendo un patron
	 *
	 * @param num
	 */
	private static void cruzNumeros(int num) {
		for (int i = 1; i < num; i++) {
			for (int j = 1; j < num; j++) {
				System.out.print(" ");
			}
			System.out.print(i + "\n");
		}
		for (int i = 1; i <= num; i++) {
			System.out.print(i);
		}
		for (int i = num - 1; i > 0; i--) {
			System.out.print(i);
		}
		System.out.println();
		for (int i = num - 1; i > 0; i--) {
			for (int j = 1; j < num; j++) {
				System.out.print(" ");
			}
			System.out.print(i + "\n");
		}
	}
}
