package practica;

public class CuadradosPerfectos {
	public static void main(String[] args) {
		primerosCuadrados(9);
	}

	/**
	 * Recibe un numero positivo (n) e imprime los primeros n cuadrados perfectos
	 * impares
	 * 
	 * @param n
	 */
	public static void primerosCuadrados(int n) {
		int cantCuadrados = 0;
		int i = 1;
		while (cantCuadrados < n) {
			int cuadrado = (int) Math.pow(i, 2);
			if (cuadrado % 2 != 0) {
				System.out.println(cuadrado);
				cantCuadrados++;
			}
			i++;

		}
	}

}
