package practica;

public class Factorial {
	public static void main(String[] args) {
		System.out.println(factorial(10));
	}
	
	/**
	 * Calcula el factorial de un numero natural
	 * @param n
	 * @return
	 */
	public static long factorial(long n) {
		if (n > 1)
			return n * factorial(n - 1);
		else
			return n;
	}
}
