package practica;

public class InvertirString {
	public static void main(String[] args) {
		System.out.println(invertirString("Hola Mundis!"));
	}

	/**
	 * Invierte un String
	 * 
	 * @param s
	 * @return
	 */
	private static String invertirString(String s) {
		String nuevoString = "";
		for (int i = s.length() - 1; i >= 0; i--) {
			nuevoString += s.charAt(i);
		}
		return nuevoString;
	}
}
