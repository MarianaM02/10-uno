package practica;

public class Matrices {
	public static void main(String[] args) {
		int[][] matrizA = { { 2, 3, 4 }, { 3, 5, 1 }, { 8, 9, 3 } };
		int[][] matrizB = { { 6, 4, 2 }, { 7, 6, 8 }, { 3, 6, 3 } };
		
		multiplicarMatrices(matrizA,matrizB);
	}

	public static int[][] multiplicarMatrices(int[][] A, int[][] B) {
		if (sePuedenMultiplicar(A,B)){
			int fil = A.length;
			int col = B[0].length;
			int[][] resultado = new int[fil][col];
			for (int i = 0; i < fil; i++) {
				for (int j = 0; j < col; j++) {
					resultado[i][j] = 0;
					///////// HACER
				}
			}
			return resultado;
		}
		return null;
	}

	public static boolean sePuedenMultiplicar(int[][] A, int[][] B) {
		return A[0].length == B.length;
	}
}
