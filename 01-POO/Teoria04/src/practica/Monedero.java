package practica;

public class Monedero {
	// atributo == variable miembro
	private double dinero;
	
	// constructor parametrizado
	public Monedero(double dineroInicial) {
		this.dinero = dineroInicial;
	}
	
	// constructor por defecto/predeterminado
	public Monedero() {
		this.dinero = 0;
	}
	
	// interfaz 
	public double consultarDinero() {
		return this.dinero;
	}

	public void meterDinero(double monto) {
		if (monto > 0)
			this.dinero += monto;
	}

	public void sacarDinero(double monto) {
		if (monto > 0 && monto <= dinero)
			this.dinero -= monto;
	}
	public static void main(String[] args) {
		Monedero m1 = new Monedero(400);
		System.out.println(m1.dinero);
	}
}
