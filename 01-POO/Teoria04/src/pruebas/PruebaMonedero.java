package pruebas;

import practica.Monedero;

public class PruebaMonedero {
	public static void main(String[] args) {
		Monedero m1 = new Monedero(400);
		System.out.println(m1.consultarDinero());
		m1.sacarDinero(500);
		System.out.println(m1.consultarDinero());
		m1.sacarDinero(212);
		System.out.println(m1.consultarDinero());
		m1.meterDinero(300);
		System.out.println(m1.consultarDinero());

	}
}
