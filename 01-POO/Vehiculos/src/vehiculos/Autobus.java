package vehiculos;

public class Autobus extends Vehiculo {

	public Persona[] pasajeros;
	private int asientoVacio = 0;

	protected Autobus(int km, Persona per, int cantAsientos) {
		super(km, per);
		this.pasajeros = new Persona[cantAsientos];
	}

	public Persona[] getPasajeros() {
		return pasajeros;
	}

	public boolean subirPasajero(Persona pasajero) {
		if (this.asientoVacio < this.pasajeros.length) {
			this.pasajeros[asientoVacio] = pasajero;
			this.asientoVacio++;
			return true;
		}
		return false;
	}
	
	private boolean tienePasajeros() {
		return this.asientoVacio > 0;
	}
	
	public boolean bajarPasajero() {
		if (tienePasajeros()) {
			this.asientoVacio--;
			return true;
		}
		return false;
	}

	@Override
	protected boolean cambiarChofer(Persona chofer) {
		if (!tienePasajeros()) {
			super.asignarChofer(chofer);
			return true;
		}
		return false;
	}
}
