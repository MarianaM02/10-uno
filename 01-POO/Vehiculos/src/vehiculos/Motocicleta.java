package vehiculos;

public class Motocicleta extends Vehiculo {

	private Persona acompaniante;

	protected Motocicleta(int km, Persona chofer) {
		super(km, chofer);
	}

	public Persona getAcompaniante() {
		return acompaniante;
	}

	private boolean tieneAcompaniante() {
		return this.acompaniante != null;
	}

	public boolean agregarAcompaniante(Persona acompaniante) {
		if (!tieneAcompaniante() && hayChofer()) {
			this.acompaniante = acompaniante;
			return true;
		}
		return false;
	}

	@Override
	public boolean cambiarChofer(Persona chofer) {
		if (!tieneAcompaniante()) {
			super.asignarChofer(chofer);
			return true;
		}
		return false;
	}
}
