package vehiculos;

public class Persona {
	private String nombre;

	public Persona() {}

	public Persona(String nombre) {
		this.nombre = nombre;
	}

	public String toString() {
		return nombre;
	}
}
