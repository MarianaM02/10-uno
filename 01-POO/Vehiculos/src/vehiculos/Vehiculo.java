package vehiculos;

public abstract class Vehiculo {
	private int kmRecorrridos;
	private Persona chofer;

	protected Vehiculo(int km, Persona per) {
		this.kmRecorrridos = km;
		asignarChofer(per);
	}

	protected int getKmRecorrridos() {
		return kmRecorrridos;
	}

	protected void viajar(int kmRecorrridos) {
		this.kmRecorrridos += kmRecorrridos;
	}

	protected Persona getChofer() {
		return chofer;
	}

	protected void asignarChofer(Persona chofer) {
		this.chofer = chofer;
	}

	protected abstract boolean cambiarChofer(Persona chofer);

	protected boolean hayChofer() {
		return this.chofer != null;
	}
}
