package vehiculos;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VehiculosTest {
	Persona cacho;
	Persona ana;
	Persona juana;
	Persona luis;
	Persona sergio;
	Persona juan;
	Motocicleta miMoto;
	Autobus unBus;
	
	@Before
	public void setUp() throws Exception {
		cacho = new Persona();
		ana = new Persona();
		juana = new Persona();
		luis = new Persona();
		sergio = new Persona();
		juan = new Persona();
	}

	@Test
	public void crearMotoTest() {
		miMoto = new Motocicleta(0, cacho);
		assertNotNull(miMoto);
	}
	
	@Test
	public void agregarAcompTest() {
		miMoto = new Motocicleta(0,  null);
		assertFalse(miMoto.agregarAcompaniante(cacho));
		
		miMoto = new Motocicleta(0,  cacho);
		assertTrue(miMoto.agregarAcompaniante(ana));
	}
	@Test
	public void crearAutobus() {
		unBus = new Autobus(0, cacho, 20);
		assertNotNull(unBus);
		assertEquals(0, unBus.getKmRecorrridos());
	}
	
	@Test
	public void cambiarChoferTest() {
		miMoto = new Motocicleta(0,  cacho);
		miMoto.agregarAcompaniante(ana);
		assertFalse(miMoto.cambiarChofer(juan));
		
		miMoto.agregarAcompaniante(null);
		assertTrue(miMoto.cambiarChofer(juan));
		
		unBus = new Autobus(0, sergio, 20);
		unBus.subirPasajero(luis);
		unBus.subirPasajero(juana);
		assertFalse(unBus.cambiarChofer(juan));
		
		unBus.bajarPasajero();
		unBus.bajarPasajero();
		assertTrue(unBus.cambiarChofer(juan));
	}

}
