package videoclub;

public interface Entregable extends Comparable<Entregable>{
	/**
	 * cambia el atributo prestado a true
	 */
	void entregar();

	/**
	 * cambia el atributo prestado a false
	 */
	void devolver();

	/**
	 * devuelve el estado del atributo prestado
	 */
	boolean isEntregado();

	/**
	 * compara las horas estimadas en los videojuegos y en las series el numero de
	 * temporadas. y en las Películas
	 */
	boolean compareTo(Object a);
}
