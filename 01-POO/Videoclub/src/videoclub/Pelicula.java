package videoclub;

public class Pelicula implements Entregable{
	
	private String titulo;
	private int anio;
	private boolean entregado = false;
	private String genero;
	private String director;
	
	public Pelicula(String titulo, int anio, String genero, String director) {
		setTitulo(titulo);
		setAnio(anio);
		setGenero(genero);
		setDirector(director);
	}

	public Pelicula() {
		this(null, 0, "no definido", null);
	}

	public Pelicula(String titulo, String director) {
		this(titulo, 0, "no definido", director);
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getAnio() {
		return anio;
	}
	public void setAnio(int anio) {
		this.anio = anio;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}

	@Override
	public String toString () {
		return "Título: " + titulo 
				+ "\nAño: "+ anio
				+ "\nGenero:" + genero
				+ "\nDirector: " + director
				+ "\nEntregada?: " + entregado;
	}

	@Override
	public void entregar() {
		if (!this.entregado) {
			this.entregado = true;
		}
	}

	@Override
	public void devolver() {
		if (this.entregado) {
			this.entregado = false;
		}
	}

	@Override
	public boolean isEntregado() {
		return this.entregado;
	}

	@Override
	public int compareTo(Entregable arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

}
