package videoclub;

public class Serie implements Entregable{
	private String titulo;
	private int nroTemporadas;
	private boolean entregado = false;
	private String genero;
	private String creador;

	public Serie(String titulo, int nroTemporadas, String genero, String creador) {
		setTitulo(titulo);
		setNroTemporadas(nroTemporadas);
		setGenero(genero);
		setCreador(creador);
	}

	public Serie() {
		this(null, 0, "no definido", null);
	}

	public Serie(String titulo, String creador) {
		this(titulo, 0, "no definido", creador);
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getNroTemporadas() {
		return nroTemporadas;
	}

	public void setNroTemporadas(int nroTemporadas) {
		this.nroTemporadas = nroTemporadas;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	@Override
	public String toString() {
		return "Título: " + titulo + "\nTemporadas: " + nroTemporadas + "\nGenero:" + genero + "\nCreador: " + creador
				+ "\nEntregada?: " + entregado;
	}

	@Override
	public void entregar() {
		if (!this.entregado) {
			this.entregado = true;
		}
	}

	@Override
	public void devolver() {
		if (this.entregado) {
			this.entregado = false;
		}
	}

	@Override
	public boolean isEntregado() {
		return this.entregado;
	}

	@Override
	public boolean compareTo(Object a) {
		return false;
	}

}
