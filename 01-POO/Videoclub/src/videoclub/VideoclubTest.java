package videoclub;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class VideoclubTest {
	Pelicula peli1;

	@Before
	public void setUp() throws Exception {
		peli1 = new Pelicula("Volver al Futuro", 1985, "Comedia/Ciencia Ficcion", "Robert Zemeckis");
	}

	@Test
	public void crearPeliTest() {
		assertNotNull(peli1);
		assertFalse(peli1.isEntregado());
	}
	
	@Test
	public void prestarPeliTest() {
		assertFalse(peli1.isEntregado());
		peli1.entregar();
		assertTrue(peli1.isEntregado());
	}

	@Test
	public void devolverPeliTest() {
		assertFalse(peli1.isEntregado());
		peli1.entregar();
		assertTrue(peli1.isEntregado());
		peli1.devolver();
		assertFalse(peli1.isEntregado());
	}
}
