package videoclub;

public class Videojuego implements Entregable{
	private String titulo;
	private int horasEstimadas;
	private boolean entregado = false;
	private String genero;
	private String compania;

	public Videojuego(String titulo, int horasEstimadas, String genero, String compania) {
		setTitulo(titulo);
		setHorasEstimadas(horasEstimadas);
		setGenero(genero);
		setCompania(compania);
	}

	public Videojuego() {
		this(null, 0, "no definido", null);
	}

	public Videojuego(String titulo, String compania) {
		this(titulo, 0, "no definido", compania);
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getHorasEstimadas() {
		return horasEstimadas;
	}

	public void setHorasEstimadas(int horasEstimadas) {
		this.horasEstimadas = horasEstimadas;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	@Override
	public String toString() {
		return "Título: " + titulo + "\nHoras Estimadas: " + horasEstimadas + "\nGenero:" + genero + "\nCompañia: "
				+ compania + "\nEntregada?: " + entregado;
	}

	@Override
	public void entregar() {
		if (!this.entregado) {
			this.entregado = true;
		}
	}

	@Override
	public void devolver() {
		if (this.entregado) {
			this.entregado = false;
		}
	}

	@Override
	public boolean isEntregado() {
		return this.entregado;
	}

	@Override
	public boolean compareTo(Object a) {
		return false;
	}
}
