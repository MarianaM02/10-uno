package ar.edu.uno.poo1.monedero;

public class Monedero {
	/*
	 * Atributos
	 */
	private Dinero dinero;
	
	/*
	 * Constructors
	 */
	
	// Constructor parametrizado
	public Monedero(Dinero dineroInicial) {
		this.dinero = dineroInicial;
	}
	
	// Constructor por defecto/predeterminado
	public Monedero() {
		this.dinero = null;
	}
	
	/*
	 * Métodos
	 */
	public double consultarDinero() {
		return this.dinero.getMonto();
	}
	
	public void sacarDinero(double monto) {
		if (monto > 0 && monto <= this.dinero.getMonto())
			this.dinero.setMonto(this.dinero.getMonto() - monto);
	}
	
	public void ponerDinero(double monto) {
		if (monto > 0)
			this.dinero.setMonto(this.dinero.getMonto() + monto);
	}
}
