package ar.edu.uno.poo1.monedero;

public class TestMonedero {	
	public static void main(String[] args) {
		
		/*
		Dinero d = new Dinero("ARS", "Pesos argentinos", 22.5);
		Monedero m1 = new Monedero(d); // Agregación
		*/
		
		Monedero m1 = new Monedero(new Dinero("ARS", "Pesos argentinos", 22.5)); // Composición
		
		// El atributo 'dinero' no es visible
		// acá, porque estamos en el cuerpo de la clase
		// TestMonedero
		// System.out.println(m1.dinero);
		
		System.out.println("El monedero m1 tiene: $" + 
						m1.consultarDinero()); // 22.5	
		m1.sacarDinero(22.5);
		System.out.println("El monedero m1 tiene: $" + 
				m1.consultarDinero()); // 0
		m1.ponerDinero(400.8);
		System.out.println("El monedero m1 tiene: $" + 
				m1.consultarDinero()); // 400.8
		m1 = null; // Destruyo al monedero
		
		// System.out.println(d.getMonto()); // 400.8	
		
	}	
}
